

package com.developers.perspective.util

import java.util

object Environemnt {
  val baseURL = scala.util.Properties.envOrElse("baseURL", "http://10.14.24.59:8080/user_service/api/ums/users/")
  val users = scala.util.Properties.envOrElse("numberOfUsers", "10")
  val maxResponseTime = scala.util.Properties.envOrElse("maxResponseTime", "5000") // in milliseconds

}