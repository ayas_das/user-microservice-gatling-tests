package com.developers.perspective.scenarios

import io.gatling.core.Predef._
import io.gatling.http.Predef._

object PostAndGetUser {

  //val usersDataSource: Array[Map[String, String]] = csv("user_credentials.csv")
  val usersDataSource = csv("user_credentials.csv").queue
  val usersDataSource1 = csv("user_information.csv").queue
  //private val usersCount: Int = usersDataSource.
  //val userString = "{\"firstName\":\"Scala\", \"lastName\":\"Java\",\"role\":\"ADMIN\"}"

  val postUserHttp = http("post user" + "${username}")
    .post("")
    .body(StringBody("""{"loginid": "${username}","passwd": "${password}","email":"harsbhtest@mm.com","mobile":"4544545","source_from":"harsh","club_type": "1","gender": "M","yob":"1989","user_preferenceState":"Maha","dummy_to_real": "2015-12-24 11:58:29","usercreationtime": "2015-12-24 11:58:29","last_login":"2015-12-24 11:58:29"}""")).asJSON
    .check(status is 201)

  val putUserHttp1 = http("put user" + "${user_id}")
    .put("""${user_id}""")
    .body(StringBody("""{"email": "testharsh", "mobile": 9769113279,"mobile_confirmation": 1,"email_confirmation": 1}"""))
    .check(status is 204)
    
  val postUser = scenario("post user")
  .feed(usersDataSource)
  .exec(postUserHttp)
    
    val putUser = scenario("put user")
    .feed(usersDataSource1)
    .exec(putUserHttp1)
}