
package com.developers.perspective.scenarios

import io.gatling.core.Predef._
import io.gatling.http.Predef._

object PostUser {

  val userString = "{\"firstName\":\"Scala\", \"lastName\":\"Java\",\"role\":\"ADMIN\"}"

  val postUserHttp = http("post user")
    .post("")
    .body(StringBody(userString))
    .check(status is 201)

  val postUserHttp1 = http("put user")
    .put("")
    .body(StringBody(userString))
    .check(status is 204)
    
  val postUser = scenario("post user")
    .exec(postUserHttp)
    
    val postUser1 = scenario("put user")
    .exec(postUserHttp1)
}