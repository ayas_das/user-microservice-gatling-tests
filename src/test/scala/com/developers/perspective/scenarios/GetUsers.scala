package com.developers.perspective.scenarios



import io.gatling.core.Predef._
import io.gatling.http.Predef._

object GetUsers {
  
  val usersDataSource = csv("user_information.csv").queue

  val getUsersHttp = http("get all users" + "${user_id}")
                        .get("""${user_id}""")
                        .queryParam("projection", "user_id,loginid,resend_email_count,account_status,yob,pin,address_Verification,addressline")
                        .check(status is 200)
                        
  val getUsersHttpsearch = http("search users" + "${user_id}")
                        .get("""${user_id}""")
                        .queryParamSeq(Seq(("projection", "user_id,loginid"),("_s",  "user_id==1523,email=='harsh.dhruv@games24x7.com'")))
                        .check(status is 200)

  val getUsers = scenario("Get All users")
.feed(usersDataSource)
.exec(getUsersHttp)

  val searchUser = scenario("Search all users")
  .feed(usersDataSource)
  .exec(getUsersHttpsearch)
}