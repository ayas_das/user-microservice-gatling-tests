var stats = {
    type: "GROUP",
name: "Global Information",
path: "",
pathFormatted: "group_missing-name-b06d1",
stats: {
    "name": "Global Information",
    "numberOfRequests": {
        "total": "82",
        "ok": "79",
        "ko": "3"
    },
    "minResponseTime": {
        "total": "138",
        "ok": "138",
        "ko": "160"
    },
    "maxResponseTime": {
        "total": "265",
        "ok": "265",
        "ko": "221"
    },
    "meanResponseTime": {
        "total": "186",
        "ok": "185",
        "ko": "195"
    },
    "standardDeviation": {
        "total": "36",
        "ok": "36",
        "ko": "25"
    },
    "percentiles1": {
        "total": "177",
        "ok": "176",
        "ko": "205"
    },
    "percentiles2": {
        "total": "215",
        "ok": "214",
        "ko": "213"
    },
    "percentiles3": {
        "total": "244",
        "ok": "244",
        "ko": "219"
    },
    "percentiles4": {
        "total": "252",
        "ok": "252",
        "ko": "220"
    },
    "group1": {
        "name": "t < 800 ms",
        "count": 79,
        "percentage": 96
    },
    "group2": {
        "name": "800 ms < t < 1200 ms",
        "count": 0,
        "percentage": 0
    },
    "group3": {
        "name": "t > 1200 ms",
        "count": 0,
        "percentage": 0
    },
    "group4": {
        "name": "failed",
        "count": 3,
        "percentage": 4
    },
    "meanNumberOfRequestsPerSecond": {
        "total": "8.17",
        "ok": "7.871",
        "ko": "0.299"
    }
},
contents: {
"req_get-all-users10-2d501": {
        type: "REQUEST",
        name: "get all users1001",
path: "get all users1001",
pathFormatted: "req_get-all-users10-2d501",
stats: {
    "name": "get all users1001",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "183",
        "ok": "183",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "183",
        "ok": "183",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "183",
        "ok": "183",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "183",
        "ok": "183",
        "ko": "-"
    },
    "percentiles2": {
        "total": "183",
        "ok": "183",
        "ko": "-"
    },
    "percentiles3": {
        "total": "183",
        "ok": "183",
        "ko": "-"
    },
    "percentiles4": {
        "total": "183",
        "ok": "183",
        "ko": "-"
    },
    "group1": {
        "name": "t < 800 ms",
        "count": 1,
        "percentage": 100
    },
    "group2": {
        "name": "800 ms < t < 1200 ms",
        "count": 0,
        "percentage": 0
    },
    "group3": {
        "name": "t > 1200 ms",
        "count": 0,
        "percentage": 0
    },
    "group4": {
        "name": "failed",
        "count": 0,
        "percentage": 0
    },
    "meanNumberOfRequestsPerSecond": {
        "total": "0.1",
        "ok": "0.1",
        "ko": "-"
    }
}
    },"req_post-useruser15-d4be1": {
        type: "REQUEST",
        name: "post useruser156",
path: "post useruser156",
pathFormatted: "req_post-useruser15-d4be1",
stats: {
    "name": "post useruser156",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "243",
        "ok": "243",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "243",
        "ok": "243",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "243",
        "ok": "243",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "243",
        "ok": "243",
        "ko": "-"
    },
    "percentiles2": {
        "total": "243",
        "ok": "243",
        "ko": "-"
    },
    "percentiles3": {
        "total": "243",
        "ok": "243",
        "ko": "-"
    },
    "percentiles4": {
        "total": "243",
        "ok": "243",
        "ko": "-"
    },
    "group1": {
        "name": "t < 800 ms",
        "count": 1,
        "percentage": 100
    },
    "group2": {
        "name": "800 ms < t < 1200 ms",
        "count": 0,
        "percentage": 0
    },
    "group3": {
        "name": "t > 1200 ms",
        "count": 0,
        "percentage": 0
    },
    "group4": {
        "name": "failed",
        "count": 0,
        "percentage": 0
    },
    "meanNumberOfRequestsPerSecond": {
        "total": "0.1",
        "ok": "0.1",
        "ko": "-"
    }
}
    },"req_search-users100-80b77": {
        type: "REQUEST",
        name: "search users1001",
path: "search users1001",
pathFormatted: "req_search-users100-80b77",
stats: {
    "name": "search users1001",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "188",
        "ok": "188",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "188",
        "ok": "188",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "188",
        "ok": "188",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "188",
        "ok": "188",
        "ko": "-"
    },
    "percentiles2": {
        "total": "188",
        "ok": "188",
        "ko": "-"
    },
    "percentiles3": {
        "total": "188",
        "ok": "188",
        "ko": "-"
    },
    "percentiles4": {
        "total": "188",
        "ok": "188",
        "ko": "-"
    },
    "group1": {
        "name": "t < 800 ms",
        "count": 1,
        "percentage": 100
    },
    "group2": {
        "name": "800 ms < t < 1200 ms",
        "count": 0,
        "percentage": 0
    },
    "group3": {
        "name": "t > 1200 ms",
        "count": 0,
        "percentage": 0
    },
    "group4": {
        "name": "failed",
        "count": 0,
        "percentage": 0
    },
    "meanNumberOfRequestsPerSecond": {
        "total": "0.1",
        "ok": "0.1",
        "ko": "-"
    }
}
    },"req_post-useruser15-14e1a": {
        type: "REQUEST",
        name: "post useruser159",
path: "post useruser159",
pathFormatted: "req_post-useruser15-14e1a",
stats: {
    "name": "post useruser159",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "244",
        "ok": "244",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "244",
        "ok": "244",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "244",
        "ok": "244",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "244",
        "ok": "244",
        "ko": "-"
    },
    "percentiles2": {
        "total": "244",
        "ok": "244",
        "ko": "-"
    },
    "percentiles3": {
        "total": "244",
        "ok": "244",
        "ko": "-"
    },
    "percentiles4": {
        "total": "244",
        "ok": "244",
        "ko": "-"
    },
    "group1": {
        "name": "t < 800 ms",
        "count": 1,
        "percentage": 100
    },
    "group2": {
        "name": "800 ms < t < 1200 ms",
        "count": 0,
        "percentage": 0
    },
    "group3": {
        "name": "t > 1200 ms",
        "count": 0,
        "percentage": 0
    },
    "group4": {
        "name": "failed",
        "count": 0,
        "percentage": 0
    },
    "meanNumberOfRequestsPerSecond": {
        "total": "0.1",
        "ok": "0.1",
        "ko": "-"
    }
}
    },"req_post-useruser15-34a58": {
        type: "REQUEST",
        name: "post useruser155",
path: "post useruser155",
pathFormatted: "req_post-useruser15-34a58",
stats: {
    "name": "post useruser155",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "244",
        "ok": "244",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "244",
        "ok": "244",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "244",
        "ok": "244",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "244",
        "ok": "244",
        "ko": "-"
    },
    "percentiles2": {
        "total": "244",
        "ok": "244",
        "ko": "-"
    },
    "percentiles3": {
        "total": "244",
        "ok": "244",
        "ko": "-"
    },
    "percentiles4": {
        "total": "244",
        "ok": "244",
        "ko": "-"
    },
    "group1": {
        "name": "t < 800 ms",
        "count": 1,
        "percentage": 100
    },
    "group2": {
        "name": "800 ms < t < 1200 ms",
        "count": 0,
        "percentage": 0
    },
    "group3": {
        "name": "t > 1200 ms",
        "count": 0,
        "percentage": 0
    },
    "group4": {
        "name": "failed",
        "count": 0,
        "percentage": 0
    },
    "meanNumberOfRequestsPerSecond": {
        "total": "0.1",
        "ok": "0.1",
        "ko": "-"
    }
}
    },"req_put-user1008-bfd50": {
        type: "REQUEST",
        name: "put user1008",
path: "put user1008",
pathFormatted: "req_put-user1008-bfd50",
stats: {
    "name": "put user1008",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "238",
        "ok": "238",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "238",
        "ok": "238",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "238",
        "ok": "238",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "238",
        "ok": "238",
        "ko": "-"
    },
    "percentiles2": {
        "total": "238",
        "ok": "238",
        "ko": "-"
    },
    "percentiles3": {
        "total": "238",
        "ok": "238",
        "ko": "-"
    },
    "percentiles4": {
        "total": "238",
        "ok": "238",
        "ko": "-"
    },
    "group1": {
        "name": "t < 800 ms",
        "count": 1,
        "percentage": 100
    },
    "group2": {
        "name": "800 ms < t < 1200 ms",
        "count": 0,
        "percentage": 0
    },
    "group3": {
        "name": "t > 1200 ms",
        "count": 0,
        "percentage": 0
    },
    "group4": {
        "name": "failed",
        "count": 0,
        "percentage": 0
    },
    "meanNumberOfRequestsPerSecond": {
        "total": "0.1",
        "ok": "0.1",
        "ko": "-"
    }
}
    },"req_put-user1007-0c5b2": {
        type: "REQUEST",
        name: "put user1007",
path: "put user1007",
pathFormatted: "req_put-user1007-0c5b2",
stats: {
    "name": "put user1007",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "230",
        "ok": "230",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "230",
        "ok": "230",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "230",
        "ok": "230",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "230",
        "ok": "230",
        "ko": "-"
    },
    "percentiles2": {
        "total": "230",
        "ok": "230",
        "ko": "-"
    },
    "percentiles3": {
        "total": "230",
        "ok": "230",
        "ko": "-"
    },
    "percentiles4": {
        "total": "230",
        "ok": "230",
        "ko": "-"
    },
    "group1": {
        "name": "t < 800 ms",
        "count": 1,
        "percentage": 100
    },
    "group2": {
        "name": "800 ms < t < 1200 ms",
        "count": 0,
        "percentage": 0
    },
    "group3": {
        "name": "t > 1200 ms",
        "count": 0,
        "percentage": 0
    },
    "group4": {
        "name": "failed",
        "count": 0,
        "percentage": 0
    },
    "meanNumberOfRequestsPerSecond": {
        "total": "0.1",
        "ok": "0.1",
        "ko": "-"
    }
}
    },"req_put-user1011-0482c": {
        type: "REQUEST",
        name: "put user1011",
path: "put user1011",
pathFormatted: "req_put-user1011-0482c",
stats: {
    "name": "put user1011",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "249",
        "ok": "249",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "249",
        "ok": "249",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "249",
        "ok": "249",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "249",
        "ok": "249",
        "ko": "-"
    },
    "percentiles2": {
        "total": "249",
        "ok": "249",
        "ko": "-"
    },
    "percentiles3": {
        "total": "249",
        "ok": "249",
        "ko": "-"
    },
    "percentiles4": {
        "total": "249",
        "ok": "249",
        "ko": "-"
    },
    "group1": {
        "name": "t < 800 ms",
        "count": 1,
        "percentage": 100
    },
    "group2": {
        "name": "800 ms < t < 1200 ms",
        "count": 0,
        "percentage": 0
    },
    "group3": {
        "name": "t > 1200 ms",
        "count": 0,
        "percentage": 0
    },
    "group4": {
        "name": "failed",
        "count": 0,
        "percentage": 0
    },
    "meanNumberOfRequestsPerSecond": {
        "total": "0.1",
        "ok": "0.1",
        "ko": "-"
    }
}
    },"req_put-user1004-f6624": {
        type: "REQUEST",
        name: "put user1004",
path: "put user1004",
pathFormatted: "req_put-user1004-f6624",
stats: {
    "name": "put user1004",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "211",
        "ok": "211",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "211",
        "ok": "211",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "211",
        "ok": "211",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "211",
        "ok": "211",
        "ko": "-"
    },
    "percentiles2": {
        "total": "211",
        "ok": "211",
        "ko": "-"
    },
    "percentiles3": {
        "total": "211",
        "ok": "211",
        "ko": "-"
    },
    "percentiles4": {
        "total": "211",
        "ok": "211",
        "ko": "-"
    },
    "group1": {
        "name": "t < 800 ms",
        "count": 1,
        "percentage": 100
    },
    "group2": {
        "name": "800 ms < t < 1200 ms",
        "count": 0,
        "percentage": 0
    },
    "group3": {
        "name": "t > 1200 ms",
        "count": 0,
        "percentage": 0
    },
    "group4": {
        "name": "failed",
        "count": 0,
        "percentage": 0
    },
    "meanNumberOfRequestsPerSecond": {
        "total": "0.1",
        "ok": "0.1",
        "ko": "-"
    }
}
    },"req_get-all-users10-9f7e7": {
        type: "REQUEST",
        name: "get all users1002",
path: "get all users1002",
pathFormatted: "req_get-all-users10-9f7e7",
stats: {
    "name": "get all users1002",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "190",
        "ok": "190",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "190",
        "ok": "190",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "190",
        "ok": "190",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "190",
        "ok": "190",
        "ko": "-"
    },
    "percentiles2": {
        "total": "190",
        "ok": "190",
        "ko": "-"
    },
    "percentiles3": {
        "total": "190",
        "ok": "190",
        "ko": "-"
    },
    "percentiles4": {
        "total": "190",
        "ok": "190",
        "ko": "-"
    },
    "group1": {
        "name": "t < 800 ms",
        "count": 1,
        "percentage": 100
    },
    "group2": {
        "name": "800 ms < t < 1200 ms",
        "count": 0,
        "percentage": 0
    },
    "group3": {
        "name": "t > 1200 ms",
        "count": 0,
        "percentage": 0
    },
    "group4": {
        "name": "failed",
        "count": 0,
        "percentage": 0
    },
    "meanNumberOfRequestsPerSecond": {
        "total": "0.1",
        "ok": "0.1",
        "ko": "-"
    }
}
    },"req_post-useruser15-73b1d": {
        type: "REQUEST",
        name: "post useruser154",
path: "post useruser154",
pathFormatted: "req_post-useruser15-73b1d",
stats: {
    "name": "post useruser154",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "242",
        "ok": "242",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "242",
        "ok": "242",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "242",
        "ok": "242",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "242",
        "ok": "242",
        "ko": "-"
    },
    "percentiles2": {
        "total": "242",
        "ok": "242",
        "ko": "-"
    },
    "percentiles3": {
        "total": "242",
        "ok": "242",
        "ko": "-"
    },
    "percentiles4": {
        "total": "242",
        "ok": "242",
        "ko": "-"
    },
    "group1": {
        "name": "t < 800 ms",
        "count": 1,
        "percentage": 100
    },
    "group2": {
        "name": "800 ms < t < 1200 ms",
        "count": 0,
        "percentage": 0
    },
    "group3": {
        "name": "t > 1200 ms",
        "count": 0,
        "percentage": 0
    },
    "group4": {
        "name": "failed",
        "count": 0,
        "percentage": 0
    },
    "meanNumberOfRequestsPerSecond": {
        "total": "0.1",
        "ok": "0.1",
        "ko": "-"
    }
}
    },"req_put-user1003-fa675": {
        type: "REQUEST",
        name: "put user1003",
path: "put user1003",
pathFormatted: "req_put-user1003-fa675",
stats: {
    "name": "put user1003",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "213",
        "ok": "213",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "213",
        "ok": "213",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "213",
        "ok": "213",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "213",
        "ok": "213",
        "ko": "-"
    },
    "percentiles2": {
        "total": "213",
        "ok": "213",
        "ko": "-"
    },
    "percentiles3": {
        "total": "213",
        "ok": "213",
        "ko": "-"
    },
    "percentiles4": {
        "total": "213",
        "ok": "213",
        "ko": "-"
    },
    "group1": {
        "name": "t < 800 ms",
        "count": 1,
        "percentage": 100
    },
    "group2": {
        "name": "800 ms < t < 1200 ms",
        "count": 0,
        "percentage": 0
    },
    "group3": {
        "name": "t > 1200 ms",
        "count": 0,
        "percentage": 0
    },
    "group4": {
        "name": "failed",
        "count": 0,
        "percentage": 0
    },
    "meanNumberOfRequestsPerSecond": {
        "total": "0.1",
        "ok": "0.1",
        "ko": "-"
    }
}
    },"req_put-user1006-9e399": {
        type: "REQUEST",
        name: "put user1006",
path: "put user1006",
pathFormatted: "req_put-user1006-9e399",
stats: {
    "name": "put user1006",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "229",
        "ok": "229",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "229",
        "ok": "229",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "229",
        "ok": "229",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "229",
        "ok": "229",
        "ko": "-"
    },
    "percentiles2": {
        "total": "229",
        "ok": "229",
        "ko": "-"
    },
    "percentiles3": {
        "total": "229",
        "ok": "229",
        "ko": "-"
    },
    "percentiles4": {
        "total": "229",
        "ok": "229",
        "ko": "-"
    },
    "group1": {
        "name": "t < 800 ms",
        "count": 1,
        "percentage": 100
    },
    "group2": {
        "name": "800 ms < t < 1200 ms",
        "count": 0,
        "percentage": 0
    },
    "group3": {
        "name": "t > 1200 ms",
        "count": 0,
        "percentage": 0
    },
    "group4": {
        "name": "failed",
        "count": 0,
        "percentage": 0
    },
    "meanNumberOfRequestsPerSecond": {
        "total": "0.1",
        "ok": "0.1",
        "ko": "-"
    }
}
    },"req_put-user1009-01870": {
        type: "REQUEST",
        name: "put user1009",
path: "put user1009",
pathFormatted: "req_put-user1009-01870",
stats: {
    "name": "put user1009",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "221",
        "ok": "221",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "221",
        "ok": "221",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "221",
        "ok": "221",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "221",
        "ok": "221",
        "ko": "-"
    },
    "percentiles2": {
        "total": "221",
        "ok": "221",
        "ko": "-"
    },
    "percentiles3": {
        "total": "221",
        "ok": "221",
        "ko": "-"
    },
    "percentiles4": {
        "total": "221",
        "ok": "221",
        "ko": "-"
    },
    "group1": {
        "name": "t < 800 ms",
        "count": 1,
        "percentage": 100
    },
    "group2": {
        "name": "800 ms < t < 1200 ms",
        "count": 0,
        "percentage": 0
    },
    "group3": {
        "name": "t > 1200 ms",
        "count": 0,
        "percentage": 0
    },
    "group4": {
        "name": "failed",
        "count": 0,
        "percentage": 0
    },
    "meanNumberOfRequestsPerSecond": {
        "total": "0.1",
        "ok": "0.1",
        "ko": "-"
    }
}
    },"req_post-useruser15-352f5": {
        type: "REQUEST",
        name: "post useruser152",
path: "post useruser152",
pathFormatted: "req_post-useruser15-352f5",
stats: {
    "name": "post useruser152",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "239",
        "ok": "239",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "239",
        "ok": "239",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "239",
        "ok": "239",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "239",
        "ok": "239",
        "ko": "-"
    },
    "percentiles2": {
        "total": "239",
        "ok": "239",
        "ko": "-"
    },
    "percentiles3": {
        "total": "239",
        "ok": "239",
        "ko": "-"
    },
    "percentiles4": {
        "total": "239",
        "ok": "239",
        "ko": "-"
    },
    "group1": {
        "name": "t < 800 ms",
        "count": 1,
        "percentage": 100
    },
    "group2": {
        "name": "800 ms < t < 1200 ms",
        "count": 0,
        "percentage": 0
    },
    "group3": {
        "name": "t > 1200 ms",
        "count": 0,
        "percentage": 0
    },
    "group4": {
        "name": "failed",
        "count": 0,
        "percentage": 0
    },
    "meanNumberOfRequestsPerSecond": {
        "total": "0.1",
        "ok": "0.1",
        "ko": "-"
    }
}
    },"req_put-user1010-533f4": {
        type: "REQUEST",
        name: "put user1010",
path: "put user1010",
pathFormatted: "req_put-user1010-533f4",
stats: {
    "name": "put user1010",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "247",
        "ok": "247",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "247",
        "ok": "247",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "247",
        "ok": "247",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "247",
        "ok": "247",
        "ko": "-"
    },
    "percentiles2": {
        "total": "247",
        "ok": "247",
        "ko": "-"
    },
    "percentiles3": {
        "total": "247",
        "ok": "247",
        "ko": "-"
    },
    "percentiles4": {
        "total": "247",
        "ok": "247",
        "ko": "-"
    },
    "group1": {
        "name": "t < 800 ms",
        "count": 1,
        "percentage": 100
    },
    "group2": {
        "name": "800 ms < t < 1200 ms",
        "count": 0,
        "percentage": 0
    },
    "group3": {
        "name": "t > 1200 ms",
        "count": 0,
        "percentage": 0
    },
    "group4": {
        "name": "failed",
        "count": 0,
        "percentage": 0
    },
    "meanNumberOfRequestsPerSecond": {
        "total": "0.1",
        "ok": "0.1",
        "ko": "-"
    }
}
    },"req_post-useruser15-b1739": {
        type: "REQUEST",
        name: "post useruser153",
path: "post useruser153",
pathFormatted: "req_post-useruser15-b1739",
stats: {
    "name": "post useruser153",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "235",
        "ok": "235",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "235",
        "ok": "235",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "235",
        "ok": "235",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "235",
        "ok": "235",
        "ko": "-"
    },
    "percentiles2": {
        "total": "235",
        "ok": "235",
        "ko": "-"
    },
    "percentiles3": {
        "total": "235",
        "ok": "235",
        "ko": "-"
    },
    "percentiles4": {
        "total": "235",
        "ok": "235",
        "ko": "-"
    },
    "group1": {
        "name": "t < 800 ms",
        "count": 1,
        "percentage": 100
    },
    "group2": {
        "name": "800 ms < t < 1200 ms",
        "count": 0,
        "percentage": 0
    },
    "group3": {
        "name": "t > 1200 ms",
        "count": 0,
        "percentage": 0
    },
    "group4": {
        "name": "failed",
        "count": 0,
        "percentage": 0
    },
    "meanNumberOfRequestsPerSecond": {
        "total": "0.1",
        "ok": "0.1",
        "ko": "-"
    }
}
    },"req_post-useruser15-d5c2a": {
        type: "REQUEST",
        name: "post useruser158",
path: "post useruser158",
pathFormatted: "req_post-useruser15-d5c2a",
stats: {
    "name": "post useruser158",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "239",
        "ok": "239",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "239",
        "ok": "239",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "239",
        "ok": "239",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "239",
        "ok": "239",
        "ko": "-"
    },
    "percentiles2": {
        "total": "239",
        "ok": "239",
        "ko": "-"
    },
    "percentiles3": {
        "total": "239",
        "ok": "239",
        "ko": "-"
    },
    "percentiles4": {
        "total": "239",
        "ok": "239",
        "ko": "-"
    },
    "group1": {
        "name": "t < 800 ms",
        "count": 1,
        "percentage": 100
    },
    "group2": {
        "name": "800 ms < t < 1200 ms",
        "count": 0,
        "percentage": 0
    },
    "group3": {
        "name": "t > 1200 ms",
        "count": 0,
        "percentage": 0
    },
    "group4": {
        "name": "failed",
        "count": 0,
        "percentage": 0
    },
    "meanNumberOfRequestsPerSecond": {
        "total": "0.1",
        "ok": "0.1",
        "ko": "-"
    }
}
    },"req_post-useruser15-2be9e": {
        type: "REQUEST",
        name: "post useruser150",
path: "post useruser150",
pathFormatted: "req_post-useruser15-2be9e",
stats: {
    "name": "post useruser150",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "226",
        "ok": "226",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "226",
        "ok": "226",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "226",
        "ok": "226",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "226",
        "ok": "226",
        "ko": "-"
    },
    "percentiles2": {
        "total": "226",
        "ok": "226",
        "ko": "-"
    },
    "percentiles3": {
        "total": "226",
        "ok": "226",
        "ko": "-"
    },
    "percentiles4": {
        "total": "226",
        "ok": "226",
        "ko": "-"
    },
    "group1": {
        "name": "t < 800 ms",
        "count": 1,
        "percentage": 100
    },
    "group2": {
        "name": "800 ms < t < 1200 ms",
        "count": 0,
        "percentage": 0
    },
    "group3": {
        "name": "t > 1200 ms",
        "count": 0,
        "percentage": 0
    },
    "group4": {
        "name": "failed",
        "count": 0,
        "percentage": 0
    },
    "meanNumberOfRequestsPerSecond": {
        "total": "0.1",
        "ok": "0.1",
        "ko": "-"
    }
}
    },"req_post-useruser15-e5aac": {
        type: "REQUEST",
        name: "post useruser157",
path: "post useruser157",
pathFormatted: "req_post-useruser15-e5aac",
stats: {
    "name": "post useruser157",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "241",
        "ok": "241",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "241",
        "ok": "241",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "241",
        "ok": "241",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "241",
        "ok": "241",
        "ko": "-"
    },
    "percentiles2": {
        "total": "241",
        "ok": "241",
        "ko": "-"
    },
    "percentiles3": {
        "total": "241",
        "ok": "241",
        "ko": "-"
    },
    "percentiles4": {
        "total": "241",
        "ok": "241",
        "ko": "-"
    },
    "group1": {
        "name": "t < 800 ms",
        "count": 1,
        "percentage": 100
    },
    "group2": {
        "name": "800 ms < t < 1200 ms",
        "count": 0,
        "percentage": 0
    },
    "group3": {
        "name": "t > 1200 ms",
        "count": 0,
        "percentage": 0
    },
    "group4": {
        "name": "failed",
        "count": 0,
        "percentage": 0
    },
    "meanNumberOfRequestsPerSecond": {
        "total": "0.1",
        "ok": "0.1",
        "ko": "-"
    }
}
    },"req_put-user1005-9cbc7": {
        type: "REQUEST",
        name: "put user1005",
path: "put user1005",
pathFormatted: "req_put-user1005-9cbc7",
stats: {
    "name": "put user1005",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "222",
        "ok": "222",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "222",
        "ok": "222",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "222",
        "ok": "222",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "222",
        "ok": "222",
        "ko": "-"
    },
    "percentiles2": {
        "total": "222",
        "ok": "222",
        "ko": "-"
    },
    "percentiles3": {
        "total": "222",
        "ok": "222",
        "ko": "-"
    },
    "percentiles4": {
        "total": "222",
        "ok": "222",
        "ko": "-"
    },
    "group1": {
        "name": "t < 800 ms",
        "count": 1,
        "percentage": 100
    },
    "group2": {
        "name": "800 ms < t < 1200 ms",
        "count": 0,
        "percentage": 0
    },
    "group3": {
        "name": "t > 1200 ms",
        "count": 0,
        "percentage": 0
    },
    "group4": {
        "name": "failed",
        "count": 0,
        "percentage": 0
    },
    "meanNumberOfRequestsPerSecond": {
        "total": "0.1",
        "ok": "0.1",
        "ko": "-"
    }
}
    },"req_put-user1012-08ffc": {
        type: "REQUEST",
        name: "put user1012",
path: "put user1012",
pathFormatted: "req_put-user1012-08ffc",
stats: {
    "name": "put user1012",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "244",
        "ok": "244",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "244",
        "ok": "244",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "244",
        "ok": "244",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "244",
        "ok": "244",
        "ko": "-"
    },
    "percentiles2": {
        "total": "244",
        "ok": "244",
        "ko": "-"
    },
    "percentiles3": {
        "total": "244",
        "ok": "244",
        "ko": "-"
    },
    "percentiles4": {
        "total": "244",
        "ok": "244",
        "ko": "-"
    },
    "group1": {
        "name": "t < 800 ms",
        "count": 1,
        "percentage": 100
    },
    "group2": {
        "name": "800 ms < t < 1200 ms",
        "count": 0,
        "percentage": 0
    },
    "group3": {
        "name": "t > 1200 ms",
        "count": 0,
        "percentage": 0
    },
    "group4": {
        "name": "failed",
        "count": 0,
        "percentage": 0
    },
    "meanNumberOfRequestsPerSecond": {
        "total": "0.1",
        "ok": "0.1",
        "ko": "-"
    }
}
    },"req_post-useruser15-f9987": {
        type: "REQUEST",
        name: "post useruser151",
path: "post useruser151",
pathFormatted: "req_post-useruser15-f9987",
stats: {
    "name": "post useruser151",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "207",
        "ok": "207",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "207",
        "ok": "207",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "207",
        "ok": "207",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "207",
        "ok": "207",
        "ko": "-"
    },
    "percentiles2": {
        "total": "207",
        "ok": "207",
        "ko": "-"
    },
    "percentiles3": {
        "total": "207",
        "ok": "207",
        "ko": "-"
    },
    "percentiles4": {
        "total": "207",
        "ok": "207",
        "ko": "-"
    },
    "group1": {
        "name": "t < 800 ms",
        "count": 1,
        "percentage": 100
    },
    "group2": {
        "name": "800 ms < t < 1200 ms",
        "count": 0,
        "percentage": 0
    },
    "group3": {
        "name": "t > 1200 ms",
        "count": 0,
        "percentage": 0
    },
    "group4": {
        "name": "failed",
        "count": 0,
        "percentage": 0
    },
    "meanNumberOfRequestsPerSecond": {
        "total": "0.1",
        "ok": "0.1",
        "ko": "-"
    }
}
    },"req_search-users100-c4d8d": {
        type: "REQUEST",
        name: "search users1002",
path: "search users1002",
pathFormatted: "req_search-users100-c4d8d",
stats: {
    "name": "search users1002",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "187",
        "ok": "187",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "187",
        "ok": "187",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "187",
        "ok": "187",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "187",
        "ok": "187",
        "ko": "-"
    },
    "percentiles2": {
        "total": "187",
        "ok": "187",
        "ko": "-"
    },
    "percentiles3": {
        "total": "187",
        "ok": "187",
        "ko": "-"
    },
    "percentiles4": {
        "total": "187",
        "ok": "187",
        "ko": "-"
    },
    "group1": {
        "name": "t < 800 ms",
        "count": 1,
        "percentage": 100
    },
    "group2": {
        "name": "800 ms < t < 1200 ms",
        "count": 0,
        "percentage": 0
    },
    "group3": {
        "name": "t > 1200 ms",
        "count": 0,
        "percentage": 0
    },
    "group4": {
        "name": "failed",
        "count": 0,
        "percentage": 0
    },
    "meanNumberOfRequestsPerSecond": {
        "total": "0.1",
        "ok": "0.1",
        "ko": "-"
    }
}
    },"req_search-users100-12ce2": {
        type: "REQUEST",
        name: "search users1003",
path: "search users1003",
pathFormatted: "req_search-users100-12ce2",
stats: {
    "name": "search users1003",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "202",
        "ok": "202",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "202",
        "ok": "202",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "202",
        "ok": "202",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "202",
        "ok": "202",
        "ko": "-"
    },
    "percentiles2": {
        "total": "202",
        "ok": "202",
        "ko": "-"
    },
    "percentiles3": {
        "total": "202",
        "ok": "202",
        "ko": "-"
    },
    "percentiles4": {
        "total": "202",
        "ok": "202",
        "ko": "-"
    },
    "group1": {
        "name": "t < 800 ms",
        "count": 1,
        "percentage": 100
    },
    "group2": {
        "name": "800 ms < t < 1200 ms",
        "count": 0,
        "percentage": 0
    },
    "group3": {
        "name": "t > 1200 ms",
        "count": 0,
        "percentage": 0
    },
    "group4": {
        "name": "failed",
        "count": 0,
        "percentage": 0
    },
    "meanNumberOfRequestsPerSecond": {
        "total": "0.1",
        "ok": "0.1",
        "ko": "-"
    }
}
    },"req_get-all-users10-e4ed2": {
        type: "REQUEST",
        name: "get all users1013",
path: "get all users1013",
pathFormatted: "req_get-all-users10-e4ed2",
stats: {
    "name": "get all users1013",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "207",
        "ok": "207",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "207",
        "ok": "207",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "207",
        "ok": "207",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "207",
        "ok": "207",
        "ko": "-"
    },
    "percentiles2": {
        "total": "207",
        "ok": "207",
        "ko": "-"
    },
    "percentiles3": {
        "total": "207",
        "ok": "207",
        "ko": "-"
    },
    "percentiles4": {
        "total": "207",
        "ok": "207",
        "ko": "-"
    },
    "group1": {
        "name": "t < 800 ms",
        "count": 1,
        "percentage": 100
    },
    "group2": {
        "name": "800 ms < t < 1200 ms",
        "count": 0,
        "percentage": 0
    },
    "group3": {
        "name": "t > 1200 ms",
        "count": 0,
        "percentage": 0
    },
    "group4": {
        "name": "failed",
        "count": 0,
        "percentage": 0
    },
    "meanNumberOfRequestsPerSecond": {
        "total": "0.1",
        "ok": "0.1",
        "ko": "-"
    }
}
    },"req_search-users100-1b271": {
        type: "REQUEST",
        name: "search users1005",
path: "search users1005",
pathFormatted: "req_search-users100-1b271",
stats: {
    "name": "search users1005",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "203",
        "ok": "203",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "203",
        "ok": "203",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "203",
        "ok": "203",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "203",
        "ok": "203",
        "ko": "-"
    },
    "percentiles2": {
        "total": "203",
        "ok": "203",
        "ko": "-"
    },
    "percentiles3": {
        "total": "203",
        "ok": "203",
        "ko": "-"
    },
    "percentiles4": {
        "total": "203",
        "ok": "203",
        "ko": "-"
    },
    "group1": {
        "name": "t < 800 ms",
        "count": 1,
        "percentage": 100
    },
    "group2": {
        "name": "800 ms < t < 1200 ms",
        "count": 0,
        "percentage": 0
    },
    "group3": {
        "name": "t > 1200 ms",
        "count": 0,
        "percentage": 0
    },
    "group4": {
        "name": "failed",
        "count": 0,
        "percentage": 0
    },
    "meanNumberOfRequestsPerSecond": {
        "total": "0.1",
        "ok": "0.1",
        "ko": "-"
    }
}
    },"req_search-users100-16f8a": {
        type: "REQUEST",
        name: "search users1004",
path: "search users1004",
pathFormatted: "req_search-users100-16f8a",
stats: {
    "name": "search users1004",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "200",
        "ok": "200",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "200",
        "ok": "200",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "200",
        "ok": "200",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "200",
        "ok": "200",
        "ko": "-"
    },
    "percentiles2": {
        "total": "200",
        "ok": "200",
        "ko": "-"
    },
    "percentiles3": {
        "total": "200",
        "ok": "200",
        "ko": "-"
    },
    "percentiles4": {
        "total": "200",
        "ok": "200",
        "ko": "-"
    },
    "group1": {
        "name": "t < 800 ms",
        "count": 1,
        "percentage": 100
    },
    "group2": {
        "name": "800 ms < t < 1200 ms",
        "count": 0,
        "percentage": 0
    },
    "group3": {
        "name": "t > 1200 ms",
        "count": 0,
        "percentage": 0
    },
    "group4": {
        "name": "failed",
        "count": 0,
        "percentage": 0
    },
    "meanNumberOfRequestsPerSecond": {
        "total": "0.1",
        "ok": "0.1",
        "ko": "-"
    }
}
    },"req_get-all-users10-a22d7": {
        type: "REQUEST",
        name: "get all users1015",
path: "get all users1015",
pathFormatted: "req_get-all-users10-a22d7",
stats: {
    "name": "get all users1015",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "265",
        "ok": "265",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "265",
        "ok": "265",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "265",
        "ok": "265",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "265",
        "ok": "265",
        "ko": "-"
    },
    "percentiles2": {
        "total": "265",
        "ok": "265",
        "ko": "-"
    },
    "percentiles3": {
        "total": "265",
        "ok": "265",
        "ko": "-"
    },
    "percentiles4": {
        "total": "265",
        "ok": "265",
        "ko": "-"
    },
    "group1": {
        "name": "t < 800 ms",
        "count": 1,
        "percentage": 100
    },
    "group2": {
        "name": "800 ms < t < 1200 ms",
        "count": 0,
        "percentage": 0
    },
    "group3": {
        "name": "t > 1200 ms",
        "count": 0,
        "percentage": 0
    },
    "group4": {
        "name": "failed",
        "count": 0,
        "percentage": 0
    },
    "meanNumberOfRequestsPerSecond": {
        "total": "0.1",
        "ok": "0.1",
        "ko": "-"
    }
}
    },"req_get-all-users10-f8bd5": {
        type: "REQUEST",
        name: "get all users1014",
path: "get all users1014",
pathFormatted: "req_get-all-users10-f8bd5",
stats: {
    "name": "get all users1014",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "198",
        "ok": "198",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "198",
        "ok": "198",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "198",
        "ok": "198",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "198",
        "ok": "198",
        "ko": "-"
    },
    "percentiles2": {
        "total": "198",
        "ok": "198",
        "ko": "-"
    },
    "percentiles3": {
        "total": "198",
        "ok": "198",
        "ko": "-"
    },
    "percentiles4": {
        "total": "198",
        "ok": "198",
        "ko": "-"
    },
    "group1": {
        "name": "t < 800 ms",
        "count": 1,
        "percentage": 100
    },
    "group2": {
        "name": "800 ms < t < 1200 ms",
        "count": 0,
        "percentage": 0
    },
    "group3": {
        "name": "t > 1200 ms",
        "count": 0,
        "percentage": 0
    },
    "group4": {
        "name": "failed",
        "count": 0,
        "percentage": 0
    },
    "meanNumberOfRequestsPerSecond": {
        "total": "0.1",
        "ok": "0.1",
        "ko": "-"
    }
}
    },"req_search-users100-0604c": {
        type: "REQUEST",
        name: "search users1006",
path: "search users1006",
pathFormatted: "req_search-users100-0604c",
stats: {
    "name": "search users1006",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "168",
        "ok": "168",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "168",
        "ok": "168",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "168",
        "ok": "168",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "168",
        "ok": "168",
        "ko": "-"
    },
    "percentiles2": {
        "total": "168",
        "ok": "168",
        "ko": "-"
    },
    "percentiles3": {
        "total": "168",
        "ok": "168",
        "ko": "-"
    },
    "percentiles4": {
        "total": "168",
        "ok": "168",
        "ko": "-"
    },
    "group1": {
        "name": "t < 800 ms",
        "count": 1,
        "percentage": 100
    },
    "group2": {
        "name": "800 ms < t < 1200 ms",
        "count": 0,
        "percentage": 0
    },
    "group3": {
        "name": "t > 1200 ms",
        "count": 0,
        "percentage": 0
    },
    "group4": {
        "name": "failed",
        "count": 0,
        "percentage": 0
    },
    "meanNumberOfRequestsPerSecond": {
        "total": "0.1",
        "ok": "0.1",
        "ko": "-"
    }
}
    },"req_get-all-users10-2731d": {
        type: "REQUEST",
        name: "get all users1016",
path: "get all users1016",
pathFormatted: "req_get-all-users10-2731d",
stats: {
    "name": "get all users1016",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "150",
        "ok": "150",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "150",
        "ok": "150",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "150",
        "ok": "150",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "150",
        "ok": "150",
        "ko": "-"
    },
    "percentiles2": {
        "total": "150",
        "ok": "150",
        "ko": "-"
    },
    "percentiles3": {
        "total": "150",
        "ok": "150",
        "ko": "-"
    },
    "percentiles4": {
        "total": "150",
        "ok": "150",
        "ko": "-"
    },
    "group1": {
        "name": "t < 800 ms",
        "count": 1,
        "percentage": 100
    },
    "group2": {
        "name": "800 ms < t < 1200 ms",
        "count": 0,
        "percentage": 0
    },
    "group3": {
        "name": "t > 1200 ms",
        "count": 0,
        "percentage": 0
    },
    "group4": {
        "name": "failed",
        "count": 0,
        "percentage": 0
    },
    "meanNumberOfRequestsPerSecond": {
        "total": "0.1",
        "ok": "0.1",
        "ko": "-"
    }
}
    },"req_get-all-users10-69152": {
        type: "REQUEST",
        name: "get all users1017",
path: "get all users1017",
pathFormatted: "req_get-all-users10-69152",
stats: {
    "name": "get all users1017",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "178",
        "ok": "178",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "178",
        "ok": "178",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "178",
        "ok": "178",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "178",
        "ok": "178",
        "ko": "-"
    },
    "percentiles2": {
        "total": "178",
        "ok": "178",
        "ko": "-"
    },
    "percentiles3": {
        "total": "178",
        "ok": "178",
        "ko": "-"
    },
    "percentiles4": {
        "total": "178",
        "ok": "178",
        "ko": "-"
    },
    "group1": {
        "name": "t < 800 ms",
        "count": 1,
        "percentage": 100
    },
    "group2": {
        "name": "800 ms < t < 1200 ms",
        "count": 0,
        "percentage": 0
    },
    "group3": {
        "name": "t > 1200 ms",
        "count": 0,
        "percentage": 0
    },
    "group4": {
        "name": "failed",
        "count": 0,
        "percentage": 0
    },
    "meanNumberOfRequestsPerSecond": {
        "total": "0.1",
        "ok": "0.1",
        "ko": "-"
    }
}
    },"req_search-users100-dc03e": {
        type: "REQUEST",
        name: "search users1007",
path: "search users1007",
pathFormatted: "req_search-users100-dc03e",
stats: {
    "name": "search users1007",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "151",
        "ok": "151",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "151",
        "ok": "151",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "151",
        "ok": "151",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "151",
        "ok": "151",
        "ko": "-"
    },
    "percentiles2": {
        "total": "151",
        "ok": "151",
        "ko": "-"
    },
    "percentiles3": {
        "total": "151",
        "ok": "151",
        "ko": "-"
    },
    "percentiles4": {
        "total": "151",
        "ok": "151",
        "ko": "-"
    },
    "group1": {
        "name": "t < 800 ms",
        "count": 1,
        "percentage": 100
    },
    "group2": {
        "name": "800 ms < t < 1200 ms",
        "count": 0,
        "percentage": 0
    },
    "group3": {
        "name": "t > 1200 ms",
        "count": 0,
        "percentage": 0
    },
    "group4": {
        "name": "failed",
        "count": 0,
        "percentage": 0
    },
    "meanNumberOfRequestsPerSecond": {
        "total": "0.1",
        "ok": "0.1",
        "ko": "-"
    }
}
    },"req_search-users100-164ac": {
        type: "REQUEST",
        name: "search users1008",
path: "search users1008",
pathFormatted: "req_search-users100-164ac",
stats: {
    "name": "search users1008",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "146",
        "ok": "146",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "146",
        "ok": "146",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "146",
        "ok": "146",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "146",
        "ok": "146",
        "ko": "-"
    },
    "percentiles2": {
        "total": "146",
        "ok": "146",
        "ko": "-"
    },
    "percentiles3": {
        "total": "146",
        "ok": "146",
        "ko": "-"
    },
    "percentiles4": {
        "total": "146",
        "ok": "146",
        "ko": "-"
    },
    "group1": {
        "name": "t < 800 ms",
        "count": 1,
        "percentage": 100
    },
    "group2": {
        "name": "800 ms < t < 1200 ms",
        "count": 0,
        "percentage": 0
    },
    "group3": {
        "name": "t > 1200 ms",
        "count": 0,
        "percentage": 0
    },
    "group4": {
        "name": "failed",
        "count": 0,
        "percentage": 0
    },
    "meanNumberOfRequestsPerSecond": {
        "total": "0.1",
        "ok": "0.1",
        "ko": "-"
    }
}
    },"req_search-users100-24fb4": {
        type: "REQUEST",
        name: "search users1009",
path: "search users1009",
pathFormatted: "req_search-users100-24fb4",
stats: {
    "name": "search users1009",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "152",
        "ok": "152",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "152",
        "ok": "152",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "152",
        "ok": "152",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "152",
        "ok": "152",
        "ko": "-"
    },
    "percentiles2": {
        "total": "152",
        "ok": "152",
        "ko": "-"
    },
    "percentiles3": {
        "total": "152",
        "ok": "152",
        "ko": "-"
    },
    "percentiles4": {
        "total": "152",
        "ok": "152",
        "ko": "-"
    },
    "group1": {
        "name": "t < 800 ms",
        "count": 1,
        "percentage": 100
    },
    "group2": {
        "name": "800 ms < t < 1200 ms",
        "count": 0,
        "percentage": 0
    },
    "group3": {
        "name": "t > 1200 ms",
        "count": 0,
        "percentage": 0
    },
    "group4": {
        "name": "failed",
        "count": 0,
        "percentage": 0
    },
    "meanNumberOfRequestsPerSecond": {
        "total": "0.1",
        "ok": "0.1",
        "ko": "-"
    }
}
    },"req_get-all-users10-c12ef": {
        type: "REQUEST",
        name: "get all users1019",
path: "get all users1019",
pathFormatted: "req_get-all-users10-c12ef",
stats: {
    "name": "get all users1019",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "212",
        "ok": "212",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "212",
        "ok": "212",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "212",
        "ok": "212",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "212",
        "ok": "212",
        "ko": "-"
    },
    "percentiles2": {
        "total": "212",
        "ok": "212",
        "ko": "-"
    },
    "percentiles3": {
        "total": "212",
        "ok": "212",
        "ko": "-"
    },
    "percentiles4": {
        "total": "212",
        "ok": "212",
        "ko": "-"
    },
    "group1": {
        "name": "t < 800 ms",
        "count": 1,
        "percentage": 100
    },
    "group2": {
        "name": "800 ms < t < 1200 ms",
        "count": 0,
        "percentage": 0
    },
    "group3": {
        "name": "t > 1200 ms",
        "count": 0,
        "percentage": 0
    },
    "group4": {
        "name": "failed",
        "count": 0,
        "percentage": 0
    },
    "meanNumberOfRequestsPerSecond": {
        "total": "0.1",
        "ok": "0.1",
        "ko": "-"
    }
}
    },"req_get-all-users10-ea8c7": {
        type: "REQUEST",
        name: "get all users1020",
path: "get all users1020",
pathFormatted: "req_get-all-users10-ea8c7",
stats: {
    "name": "get all users1020",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "143",
        "ok": "143",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "143",
        "ok": "143",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "143",
        "ok": "143",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "143",
        "ok": "143",
        "ko": "-"
    },
    "percentiles2": {
        "total": "143",
        "ok": "143",
        "ko": "-"
    },
    "percentiles3": {
        "total": "143",
        "ok": "143",
        "ko": "-"
    },
    "percentiles4": {
        "total": "143",
        "ok": "143",
        "ko": "-"
    },
    "group1": {
        "name": "t < 800 ms",
        "count": 1,
        "percentage": 100
    },
    "group2": {
        "name": "800 ms < t < 1200 ms",
        "count": 0,
        "percentage": 0
    },
    "group3": {
        "name": "t > 1200 ms",
        "count": 0,
        "percentage": 0
    },
    "group4": {
        "name": "failed",
        "count": 0,
        "percentage": 0
    },
    "meanNumberOfRequestsPerSecond": {
        "total": "0.1",
        "ok": "0.1",
        "ko": "-"
    }
}
    },"req_get-all-users10-61586": {
        type: "REQUEST",
        name: "get all users1018",
path: "get all users1018",
pathFormatted: "req_get-all-users10-61586",
stats: {
    "name": "get all users1018",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "201",
        "ok": "201",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "201",
        "ok": "201",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "201",
        "ok": "201",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "201",
        "ok": "201",
        "ko": "-"
    },
    "percentiles2": {
        "total": "201",
        "ok": "201",
        "ko": "-"
    },
    "percentiles3": {
        "total": "201",
        "ok": "201",
        "ko": "-"
    },
    "percentiles4": {
        "total": "201",
        "ok": "201",
        "ko": "-"
    },
    "group1": {
        "name": "t < 800 ms",
        "count": 1,
        "percentage": 100
    },
    "group2": {
        "name": "800 ms < t < 1200 ms",
        "count": 0,
        "percentage": 0
    },
    "group3": {
        "name": "t > 1200 ms",
        "count": 0,
        "percentage": 0
    },
    "group4": {
        "name": "failed",
        "count": 0,
        "percentage": 0
    },
    "meanNumberOfRequestsPerSecond": {
        "total": "0.1",
        "ok": "0.1",
        "ko": "-"
    }
}
    },"req_search-users101-74455": {
        type: "REQUEST",
        name: "search users1010",
path: "search users1010",
pathFormatted: "req_search-users101-74455",
stats: {
    "name": "search users1010",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "146",
        "ok": "146",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "146",
        "ok": "146",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "146",
        "ok": "146",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "146",
        "ok": "146",
        "ko": "-"
    },
    "percentiles2": {
        "total": "146",
        "ok": "146",
        "ko": "-"
    },
    "percentiles3": {
        "total": "146",
        "ok": "146",
        "ko": "-"
    },
    "percentiles4": {
        "total": "146",
        "ok": "146",
        "ko": "-"
    },
    "group1": {
        "name": "t < 800 ms",
        "count": 1,
        "percentage": 100
    },
    "group2": {
        "name": "800 ms < t < 1200 ms",
        "count": 0,
        "percentage": 0
    },
    "group3": {
        "name": "t > 1200 ms",
        "count": 0,
        "percentage": 0
    },
    "group4": {
        "name": "failed",
        "count": 0,
        "percentage": 0
    },
    "meanNumberOfRequestsPerSecond": {
        "total": "0.1",
        "ok": "0.1",
        "ko": "-"
    }
}
    },"req_search-users101-fe1bc": {
        type: "REQUEST",
        name: "search users1012",
path: "search users1012",
pathFormatted: "req_search-users101-fe1bc",
stats: {
    "name": "search users1012",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "168",
        "ok": "168",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "168",
        "ok": "168",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "168",
        "ok": "168",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "168",
        "ok": "168",
        "ko": "-"
    },
    "percentiles2": {
        "total": "168",
        "ok": "168",
        "ko": "-"
    },
    "percentiles3": {
        "total": "168",
        "ok": "168",
        "ko": "-"
    },
    "percentiles4": {
        "total": "168",
        "ok": "168",
        "ko": "-"
    },
    "group1": {
        "name": "t < 800 ms",
        "count": 1,
        "percentage": 100
    },
    "group2": {
        "name": "800 ms < t < 1200 ms",
        "count": 0,
        "percentage": 0
    },
    "group3": {
        "name": "t > 1200 ms",
        "count": 0,
        "percentage": 0
    },
    "group4": {
        "name": "failed",
        "count": 0,
        "percentage": 0
    },
    "meanNumberOfRequestsPerSecond": {
        "total": "0.1",
        "ok": "0.1",
        "ko": "-"
    }
}
    },"req_get-all-users10-6cf0e": {
        type: "REQUEST",
        name: "get all users1021",
path: "get all users1021",
pathFormatted: "req_get-all-users10-6cf0e",
stats: {
    "name": "get all users1021",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "144",
        "ok": "144",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "144",
        "ok": "144",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "144",
        "ok": "144",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "144",
        "ok": "144",
        "ko": "-"
    },
    "percentiles2": {
        "total": "144",
        "ok": "144",
        "ko": "-"
    },
    "percentiles3": {
        "total": "144",
        "ok": "144",
        "ko": "-"
    },
    "percentiles4": {
        "total": "144",
        "ok": "144",
        "ko": "-"
    },
    "group1": {
        "name": "t < 800 ms",
        "count": 1,
        "percentage": 100
    },
    "group2": {
        "name": "800 ms < t < 1200 ms",
        "count": 0,
        "percentage": 0
    },
    "group3": {
        "name": "t > 1200 ms",
        "count": 0,
        "percentage": 0
    },
    "group4": {
        "name": "failed",
        "count": 0,
        "percentage": 0
    },
    "meanNumberOfRequestsPerSecond": {
        "total": "0.1",
        "ok": "0.1",
        "ko": "-"
    }
}
    },"req_get-all-users10-26a95": {
        type: "REQUEST",
        name: "get all users1022",
path: "get all users1022",
pathFormatted: "req_get-all-users10-26a95",
stats: {
    "name": "get all users1022",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "159",
        "ok": "159",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "159",
        "ok": "159",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "159",
        "ok": "159",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "159",
        "ok": "159",
        "ko": "-"
    },
    "percentiles2": {
        "total": "159",
        "ok": "159",
        "ko": "-"
    },
    "percentiles3": {
        "total": "159",
        "ok": "159",
        "ko": "-"
    },
    "percentiles4": {
        "total": "159",
        "ok": "159",
        "ko": "-"
    },
    "group1": {
        "name": "t < 800 ms",
        "count": 1,
        "percentage": 100
    },
    "group2": {
        "name": "800 ms < t < 1200 ms",
        "count": 0,
        "percentage": 0
    },
    "group3": {
        "name": "t > 1200 ms",
        "count": 0,
        "percentage": 0
    },
    "group4": {
        "name": "failed",
        "count": 0,
        "percentage": 0
    },
    "meanNumberOfRequestsPerSecond": {
        "total": "0.1",
        "ok": "0.1",
        "ko": "-"
    }
}
    },"req_get-all-users10-23522": {
        type: "REQUEST",
        name: "get all users1023",
path: "get all users1023",
pathFormatted: "req_get-all-users10-23522",
stats: {
    "name": "get all users1023",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "184",
        "ok": "184",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "184",
        "ok": "184",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "184",
        "ok": "184",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "184",
        "ok": "184",
        "ko": "-"
    },
    "percentiles2": {
        "total": "184",
        "ok": "184",
        "ko": "-"
    },
    "percentiles3": {
        "total": "184",
        "ok": "184",
        "ko": "-"
    },
    "percentiles4": {
        "total": "184",
        "ok": "184",
        "ko": "-"
    },
    "group1": {
        "name": "t < 800 ms",
        "count": 1,
        "percentage": 100
    },
    "group2": {
        "name": "800 ms < t < 1200 ms",
        "count": 0,
        "percentage": 0
    },
    "group3": {
        "name": "t > 1200 ms",
        "count": 0,
        "percentage": 0
    },
    "group4": {
        "name": "failed",
        "count": 0,
        "percentage": 0
    },
    "meanNumberOfRequestsPerSecond": {
        "total": "0.1",
        "ok": "0.1",
        "ko": "-"
    }
}
    },"req_search-users101-40161": {
        type: "REQUEST",
        name: "search users1013",
path: "search users1013",
pathFormatted: "req_search-users101-40161",
stats: {
    "name": "search users1013",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "157",
        "ok": "157",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "157",
        "ok": "157",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "157",
        "ok": "157",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "157",
        "ok": "157",
        "ko": "-"
    },
    "percentiles2": {
        "total": "157",
        "ok": "157",
        "ko": "-"
    },
    "percentiles3": {
        "total": "157",
        "ok": "157",
        "ko": "-"
    },
    "percentiles4": {
        "total": "157",
        "ok": "157",
        "ko": "-"
    },
    "group1": {
        "name": "t < 800 ms",
        "count": 1,
        "percentage": 100
    },
    "group2": {
        "name": "800 ms < t < 1200 ms",
        "count": 0,
        "percentage": 0
    },
    "group3": {
        "name": "t > 1200 ms",
        "count": 0,
        "percentage": 0
    },
    "group4": {
        "name": "failed",
        "count": 0,
        "percentage": 0
    },
    "meanNumberOfRequestsPerSecond": {
        "total": "0.1",
        "ok": "0.1",
        "ko": "-"
    }
}
    },"req_search-users101-bd7df": {
        type: "REQUEST",
        name: "search users1011",
path: "search users1011",
pathFormatted: "req_search-users101-bd7df",
stats: {
    "name": "search users1011",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "141",
        "ok": "141",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "141",
        "ok": "141",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "141",
        "ok": "141",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "141",
        "ok": "141",
        "ko": "-"
    },
    "percentiles2": {
        "total": "141",
        "ok": "141",
        "ko": "-"
    },
    "percentiles3": {
        "total": "141",
        "ok": "141",
        "ko": "-"
    },
    "percentiles4": {
        "total": "141",
        "ok": "141",
        "ko": "-"
    },
    "group1": {
        "name": "t < 800 ms",
        "count": 1,
        "percentage": 100
    },
    "group2": {
        "name": "800 ms < t < 1200 ms",
        "count": 0,
        "percentage": 0
    },
    "group3": {
        "name": "t > 1200 ms",
        "count": 0,
        "percentage": 0
    },
    "group4": {
        "name": "failed",
        "count": 0,
        "percentage": 0
    },
    "meanNumberOfRequestsPerSecond": {
        "total": "0.1",
        "ok": "0.1",
        "ko": "-"
    }
}
    },"req_search-users101-30d0c": {
        type: "REQUEST",
        name: "search users1015",
path: "search users1015",
pathFormatted: "req_search-users101-30d0c",
stats: {
    "name": "search users1015",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "176",
        "ok": "176",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "176",
        "ok": "176",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "176",
        "ok": "176",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "176",
        "ok": "176",
        "ko": "-"
    },
    "percentiles2": {
        "total": "176",
        "ok": "176",
        "ko": "-"
    },
    "percentiles3": {
        "total": "176",
        "ok": "176",
        "ko": "-"
    },
    "percentiles4": {
        "total": "176",
        "ok": "176",
        "ko": "-"
    },
    "group1": {
        "name": "t < 800 ms",
        "count": 1,
        "percentage": 100
    },
    "group2": {
        "name": "800 ms < t < 1200 ms",
        "count": 0,
        "percentage": 0
    },
    "group3": {
        "name": "t > 1200 ms",
        "count": 0,
        "percentage": 0
    },
    "group4": {
        "name": "failed",
        "count": 0,
        "percentage": 0
    },
    "meanNumberOfRequestsPerSecond": {
        "total": "0.1",
        "ok": "0.1",
        "ko": "-"
    }
}
    },"req_search-users101-065c5": {
        type: "REQUEST",
        name: "search users1014",
path: "search users1014",
pathFormatted: "req_search-users101-065c5",
stats: {
    "name": "search users1014",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "165",
        "ok": "165",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "165",
        "ok": "165",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "165",
        "ok": "165",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "165",
        "ok": "165",
        "ko": "-"
    },
    "percentiles2": {
        "total": "165",
        "ok": "165",
        "ko": "-"
    },
    "percentiles3": {
        "total": "165",
        "ok": "165",
        "ko": "-"
    },
    "percentiles4": {
        "total": "165",
        "ok": "165",
        "ko": "-"
    },
    "group1": {
        "name": "t < 800 ms",
        "count": 1,
        "percentage": 100
    },
    "group2": {
        "name": "800 ms < t < 1200 ms",
        "count": 0,
        "percentage": 0
    },
    "group3": {
        "name": "t > 1200 ms",
        "count": 0,
        "percentage": 0
    },
    "group4": {
        "name": "failed",
        "count": 0,
        "percentage": 0
    },
    "meanNumberOfRequestsPerSecond": {
        "total": "0.1",
        "ok": "0.1",
        "ko": "-"
    }
}
    },"req_get-all-users10-bee50": {
        type: "REQUEST",
        name: "get all users1026",
path: "get all users1026",
pathFormatted: "req_get-all-users10-bee50",
stats: {
    "name": "get all users1026",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "206",
        "ok": "206",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "206",
        "ok": "206",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "206",
        "ok": "206",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "206",
        "ok": "206",
        "ko": "-"
    },
    "percentiles2": {
        "total": "206",
        "ok": "206",
        "ko": "-"
    },
    "percentiles3": {
        "total": "206",
        "ok": "206",
        "ko": "-"
    },
    "percentiles4": {
        "total": "206",
        "ok": "206",
        "ko": "-"
    },
    "group1": {
        "name": "t < 800 ms",
        "count": 1,
        "percentage": 100
    },
    "group2": {
        "name": "800 ms < t < 1200 ms",
        "count": 0,
        "percentage": 0
    },
    "group3": {
        "name": "t > 1200 ms",
        "count": 0,
        "percentage": 0
    },
    "group4": {
        "name": "failed",
        "count": 0,
        "percentage": 0
    },
    "meanNumberOfRequestsPerSecond": {
        "total": "0.1",
        "ok": "0.1",
        "ko": "-"
    }
}
    },"req_get-all-users10-61c01": {
        type: "REQUEST",
        name: "get all users1024",
path: "get all users1024",
pathFormatted: "req_get-all-users10-61c01",
stats: {
    "name": "get all users1024",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "172",
        "ok": "172",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "172",
        "ok": "172",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "172",
        "ok": "172",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "172",
        "ok": "172",
        "ko": "-"
    },
    "percentiles2": {
        "total": "172",
        "ok": "172",
        "ko": "-"
    },
    "percentiles3": {
        "total": "172",
        "ok": "172",
        "ko": "-"
    },
    "percentiles4": {
        "total": "172",
        "ok": "172",
        "ko": "-"
    },
    "group1": {
        "name": "t < 800 ms",
        "count": 1,
        "percentage": 100
    },
    "group2": {
        "name": "800 ms < t < 1200 ms",
        "count": 0,
        "percentage": 0
    },
    "group3": {
        "name": "t > 1200 ms",
        "count": 0,
        "percentage": 0
    },
    "group4": {
        "name": "failed",
        "count": 0,
        "percentage": 0
    },
    "meanNumberOfRequestsPerSecond": {
        "total": "0.1",
        "ok": "0.1",
        "ko": "-"
    }
}
    },"req_get-all-users10-aa2d7": {
        type: "REQUEST",
        name: "get all users1025",
path: "get all users1025",
pathFormatted: "req_get-all-users10-aa2d7",
stats: {
    "name": "get all users1025",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "173",
        "ok": "173",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "173",
        "ok": "173",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "173",
        "ok": "173",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "173",
        "ok": "173",
        "ko": "-"
    },
    "percentiles2": {
        "total": "173",
        "ok": "173",
        "ko": "-"
    },
    "percentiles3": {
        "total": "173",
        "ok": "173",
        "ko": "-"
    },
    "percentiles4": {
        "total": "173",
        "ok": "173",
        "ko": "-"
    },
    "group1": {
        "name": "t < 800 ms",
        "count": 1,
        "percentage": 100
    },
    "group2": {
        "name": "800 ms < t < 1200 ms",
        "count": 0,
        "percentage": 0
    },
    "group3": {
        "name": "t > 1200 ms",
        "count": 0,
        "percentage": 0
    },
    "group4": {
        "name": "failed",
        "count": 0,
        "percentage": 0
    },
    "meanNumberOfRequestsPerSecond": {
        "total": "0.1",
        "ok": "0.1",
        "ko": "-"
    }
}
    },"req_search-users101-74607": {
        type: "REQUEST",
        name: "search users1016",
path: "search users1016",
pathFormatted: "req_search-users101-74607",
stats: {
    "name": "search users1016",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "216",
        "ok": "216",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "216",
        "ok": "216",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "216",
        "ok": "216",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "216",
        "ok": "216",
        "ko": "-"
    },
    "percentiles2": {
        "total": "216",
        "ok": "216",
        "ko": "-"
    },
    "percentiles3": {
        "total": "216",
        "ok": "216",
        "ko": "-"
    },
    "percentiles4": {
        "total": "216",
        "ok": "216",
        "ko": "-"
    },
    "group1": {
        "name": "t < 800 ms",
        "count": 1,
        "percentage": 100
    },
    "group2": {
        "name": "800 ms < t < 1200 ms",
        "count": 0,
        "percentage": 0
    },
    "group3": {
        "name": "t > 1200 ms",
        "count": 0,
        "percentage": 0
    },
    "group4": {
        "name": "failed",
        "count": 0,
        "percentage": 0
    },
    "meanNumberOfRequestsPerSecond": {
        "total": "0.1",
        "ok": "0.1",
        "ko": "-"
    }
}
    },"req_search-users101-cfa4d": {
        type: "REQUEST",
        name: "search users1018",
path: "search users1018",
pathFormatted: "req_search-users101-cfa4d",
stats: {
    "name": "search users1018",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "153",
        "ok": "153",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "153",
        "ok": "153",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "153",
        "ok": "153",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "153",
        "ok": "153",
        "ko": "-"
    },
    "percentiles2": {
        "total": "153",
        "ok": "153",
        "ko": "-"
    },
    "percentiles3": {
        "total": "153",
        "ok": "153",
        "ko": "-"
    },
    "percentiles4": {
        "total": "153",
        "ok": "153",
        "ko": "-"
    },
    "group1": {
        "name": "t < 800 ms",
        "count": 1,
        "percentage": 100
    },
    "group2": {
        "name": "800 ms < t < 1200 ms",
        "count": 0,
        "percentage": 0
    },
    "group3": {
        "name": "t > 1200 ms",
        "count": 0,
        "percentage": 0
    },
    "group4": {
        "name": "failed",
        "count": 0,
        "percentage": 0
    },
    "meanNumberOfRequestsPerSecond": {
        "total": "0.1",
        "ok": "0.1",
        "ko": "-"
    }
}
    },"req_get-all-users10-d3547": {
        type: "REQUEST",
        name: "get all users1028",
path: "get all users1028",
pathFormatted: "req_get-all-users10-d3547",
stats: {
    "name": "get all users1028",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "144",
        "ok": "144",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "144",
        "ok": "144",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "144",
        "ok": "144",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "144",
        "ok": "144",
        "ko": "-"
    },
    "percentiles2": {
        "total": "144",
        "ok": "144",
        "ko": "-"
    },
    "percentiles3": {
        "total": "144",
        "ok": "144",
        "ko": "-"
    },
    "percentiles4": {
        "total": "144",
        "ok": "144",
        "ko": "-"
    },
    "group1": {
        "name": "t < 800 ms",
        "count": 1,
        "percentage": 100
    },
    "group2": {
        "name": "800 ms < t < 1200 ms",
        "count": 0,
        "percentage": 0
    },
    "group3": {
        "name": "t > 1200 ms",
        "count": 0,
        "percentage": 0
    },
    "group4": {
        "name": "failed",
        "count": 0,
        "percentage": 0
    },
    "meanNumberOfRequestsPerSecond": {
        "total": "0.1",
        "ok": "0.1",
        "ko": "-"
    }
}
    },"req_get-all-users10-d8d6b": {
        type: "REQUEST",
        name: "get all users1030",
path: "get all users1030",
pathFormatted: "req_get-all-users10-d8d6b",
stats: {
    "name": "get all users1030",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "155",
        "ok": "155",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "155",
        "ok": "155",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "155",
        "ok": "155",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "155",
        "ok": "155",
        "ko": "-"
    },
    "percentiles2": {
        "total": "155",
        "ok": "155",
        "ko": "-"
    },
    "percentiles3": {
        "total": "155",
        "ok": "155",
        "ko": "-"
    },
    "percentiles4": {
        "total": "155",
        "ok": "155",
        "ko": "-"
    },
    "group1": {
        "name": "t < 800 ms",
        "count": 1,
        "percentage": 100
    },
    "group2": {
        "name": "800 ms < t < 1200 ms",
        "count": 0,
        "percentage": 0
    },
    "group3": {
        "name": "t > 1200 ms",
        "count": 0,
        "percentage": 0
    },
    "group4": {
        "name": "failed",
        "count": 0,
        "percentage": 0
    },
    "meanNumberOfRequestsPerSecond": {
        "total": "0.1",
        "ok": "0.1",
        "ko": "-"
    }
}
    },"req_search-users101-f43c2": {
        type: "REQUEST",
        name: "search users1019",
path: "search users1019",
pathFormatted: "req_search-users101-f43c2",
stats: {
    "name": "search users1019",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "146",
        "ok": "146",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "146",
        "ok": "146",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "146",
        "ok": "146",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "146",
        "ok": "146",
        "ko": "-"
    },
    "percentiles2": {
        "total": "146",
        "ok": "146",
        "ko": "-"
    },
    "percentiles3": {
        "total": "146",
        "ok": "146",
        "ko": "-"
    },
    "percentiles4": {
        "total": "146",
        "ok": "146",
        "ko": "-"
    },
    "group1": {
        "name": "t < 800 ms",
        "count": 1,
        "percentage": 100
    },
    "group2": {
        "name": "800 ms < t < 1200 ms",
        "count": 0,
        "percentage": 0
    },
    "group3": {
        "name": "t > 1200 ms",
        "count": 0,
        "percentage": 0
    },
    "group4": {
        "name": "failed",
        "count": 0,
        "percentage": 0
    },
    "meanNumberOfRequestsPerSecond": {
        "total": "0.1",
        "ok": "0.1",
        "ko": "-"
    }
}
    },"req_search-users102-a3bf7": {
        type: "REQUEST",
        name: "search users1020",
path: "search users1020",
pathFormatted: "req_search-users102-a3bf7",
stats: {
    "name": "search users1020",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "150",
        "ok": "150",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "150",
        "ok": "150",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "150",
        "ok": "150",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "150",
        "ok": "150",
        "ko": "-"
    },
    "percentiles2": {
        "total": "150",
        "ok": "150",
        "ko": "-"
    },
    "percentiles3": {
        "total": "150",
        "ok": "150",
        "ko": "-"
    },
    "percentiles4": {
        "total": "150",
        "ok": "150",
        "ko": "-"
    },
    "group1": {
        "name": "t < 800 ms",
        "count": 1,
        "percentage": 100
    },
    "group2": {
        "name": "800 ms < t < 1200 ms",
        "count": 0,
        "percentage": 0
    },
    "group3": {
        "name": "t > 1200 ms",
        "count": 0,
        "percentage": 0
    },
    "group4": {
        "name": "failed",
        "count": 0,
        "percentage": 0
    },
    "meanNumberOfRequestsPerSecond": {
        "total": "0.1",
        "ok": "0.1",
        "ko": "-"
    }
}
    },"req_get-all-users10-6a0f3": {
        type: "REQUEST",
        name: "get all users1027",
path: "get all users1027",
pathFormatted: "req_get-all-users10-6a0f3",
stats: {
    "name": "get all users1027",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "138",
        "ok": "138",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "138",
        "ok": "138",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "138",
        "ok": "138",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "138",
        "ok": "138",
        "ko": "-"
    },
    "percentiles2": {
        "total": "138",
        "ok": "138",
        "ko": "-"
    },
    "percentiles3": {
        "total": "138",
        "ok": "138",
        "ko": "-"
    },
    "percentiles4": {
        "total": "138",
        "ok": "138",
        "ko": "-"
    },
    "group1": {
        "name": "t < 800 ms",
        "count": 1,
        "percentage": 100
    },
    "group2": {
        "name": "800 ms < t < 1200 ms",
        "count": 0,
        "percentage": 0
    },
    "group3": {
        "name": "t > 1200 ms",
        "count": 0,
        "percentage": 0
    },
    "group4": {
        "name": "failed",
        "count": 0,
        "percentage": 0
    },
    "meanNumberOfRequestsPerSecond": {
        "total": "0.1",
        "ok": "0.1",
        "ko": "-"
    }
}
    },"req_get-all-users10-c026a": {
        type: "REQUEST",
        name: "get all users1029",
path: "get all users1029",
pathFormatted: "req_get-all-users10-c026a",
stats: {
    "name": "get all users1029",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "152",
        "ok": "152",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "152",
        "ok": "152",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "152",
        "ok": "152",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "152",
        "ok": "152",
        "ko": "-"
    },
    "percentiles2": {
        "total": "152",
        "ok": "152",
        "ko": "-"
    },
    "percentiles3": {
        "total": "152",
        "ok": "152",
        "ko": "-"
    },
    "percentiles4": {
        "total": "152",
        "ok": "152",
        "ko": "-"
    },
    "group1": {
        "name": "t < 800 ms",
        "count": 1,
        "percentage": 100
    },
    "group2": {
        "name": "800 ms < t < 1200 ms",
        "count": 0,
        "percentage": 0
    },
    "group3": {
        "name": "t > 1200 ms",
        "count": 0,
        "percentage": 0
    },
    "group4": {
        "name": "failed",
        "count": 0,
        "percentage": 0
    },
    "meanNumberOfRequestsPerSecond": {
        "total": "0.1",
        "ok": "0.1",
        "ko": "-"
    }
}
    },"req_search-users101-c84e5": {
        type: "REQUEST",
        name: "search users1017",
path: "search users1017",
pathFormatted: "req_search-users101-c84e5",
stats: {
    "name": "search users1017",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "140",
        "ok": "140",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "140",
        "ok": "140",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "140",
        "ok": "140",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "140",
        "ok": "140",
        "ko": "-"
    },
    "percentiles2": {
        "total": "140",
        "ok": "140",
        "ko": "-"
    },
    "percentiles3": {
        "total": "140",
        "ok": "140",
        "ko": "-"
    },
    "percentiles4": {
        "total": "140",
        "ok": "140",
        "ko": "-"
    },
    "group1": {
        "name": "t < 800 ms",
        "count": 1,
        "percentage": 100
    },
    "group2": {
        "name": "800 ms < t < 1200 ms",
        "count": 0,
        "percentage": 0
    },
    "group3": {
        "name": "t > 1200 ms",
        "count": 0,
        "percentage": 0
    },
    "group4": {
        "name": "failed",
        "count": 0,
        "percentage": 0
    },
    "meanNumberOfRequestsPerSecond": {
        "total": "0.1",
        "ok": "0.1",
        "ko": "-"
    }
}
    },"req_search-users102-a9b54": {
        type: "REQUEST",
        name: "search users1021",
path: "search users1021",
pathFormatted: "req_search-users102-a9b54",
stats: {
    "name": "search users1021",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "157",
        "ok": "157",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "157",
        "ok": "157",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "157",
        "ok": "157",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "157",
        "ok": "157",
        "ko": "-"
    },
    "percentiles2": {
        "total": "157",
        "ok": "157",
        "ko": "-"
    },
    "percentiles3": {
        "total": "157",
        "ok": "157",
        "ko": "-"
    },
    "percentiles4": {
        "total": "157",
        "ok": "157",
        "ko": "-"
    },
    "group1": {
        "name": "t < 800 ms",
        "count": 1,
        "percentage": 100
    },
    "group2": {
        "name": "800 ms < t < 1200 ms",
        "count": 0,
        "percentage": 0
    },
    "group3": {
        "name": "t > 1200 ms",
        "count": 0,
        "percentage": 0
    },
    "group4": {
        "name": "failed",
        "count": 0,
        "percentage": 0
    },
    "meanNumberOfRequestsPerSecond": {
        "total": "0.1",
        "ok": "0.1",
        "ko": "-"
    }
}
    },"req_search-users102-7f030": {
        type: "REQUEST",
        name: "search users1022",
path: "search users1022",
pathFormatted: "req_search-users102-7f030",
stats: {
    "name": "search users1022",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "156",
        "ok": "156",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "156",
        "ok": "156",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "156",
        "ok": "156",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "156",
        "ok": "156",
        "ko": "-"
    },
    "percentiles2": {
        "total": "156",
        "ok": "156",
        "ko": "-"
    },
    "percentiles3": {
        "total": "156",
        "ok": "156",
        "ko": "-"
    },
    "percentiles4": {
        "total": "156",
        "ok": "156",
        "ko": "-"
    },
    "group1": {
        "name": "t < 800 ms",
        "count": 1,
        "percentage": 100
    },
    "group2": {
        "name": "800 ms < t < 1200 ms",
        "count": 0,
        "percentage": 0
    },
    "group3": {
        "name": "t > 1200 ms",
        "count": 0,
        "percentage": 0
    },
    "group4": {
        "name": "failed",
        "count": 0,
        "percentage": 0
    },
    "meanNumberOfRequestsPerSecond": {
        "total": "0.1",
        "ok": "0.1",
        "ko": "-"
    }
}
    },"req_search-users102-70cdc": {
        type: "REQUEST",
        name: "search users1023",
path: "search users1023",
pathFormatted: "req_search-users102-70cdc",
stats: {
    "name": "search users1023",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "141",
        "ok": "141",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "141",
        "ok": "141",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "141",
        "ok": "141",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "141",
        "ok": "141",
        "ko": "-"
    },
    "percentiles2": {
        "total": "141",
        "ok": "141",
        "ko": "-"
    },
    "percentiles3": {
        "total": "141",
        "ok": "141",
        "ko": "-"
    },
    "percentiles4": {
        "total": "141",
        "ok": "141",
        "ko": "-"
    },
    "group1": {
        "name": "t < 800 ms",
        "count": 1,
        "percentage": 100
    },
    "group2": {
        "name": "800 ms < t < 1200 ms",
        "count": 0,
        "percentage": 0
    },
    "group3": {
        "name": "t > 1200 ms",
        "count": 0,
        "percentage": 0
    },
    "group4": {
        "name": "failed",
        "count": 0,
        "percentage": 0
    },
    "meanNumberOfRequestsPerSecond": {
        "total": "0.1",
        "ok": "0.1",
        "ko": "-"
    }
}
    },"req_search-users102-b4ab8": {
        type: "REQUEST",
        name: "search users1024",
path: "search users1024",
pathFormatted: "req_search-users102-b4ab8",
stats: {
    "name": "search users1024",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "138",
        "ok": "138",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "138",
        "ok": "138",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "138",
        "ok": "138",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "138",
        "ok": "138",
        "ko": "-"
    },
    "percentiles2": {
        "total": "138",
        "ok": "138",
        "ko": "-"
    },
    "percentiles3": {
        "total": "138",
        "ok": "138",
        "ko": "-"
    },
    "percentiles4": {
        "total": "138",
        "ok": "138",
        "ko": "-"
    },
    "group1": {
        "name": "t < 800 ms",
        "count": 1,
        "percentage": 100
    },
    "group2": {
        "name": "800 ms < t < 1200 ms",
        "count": 0,
        "percentage": 0
    },
    "group3": {
        "name": "t > 1200 ms",
        "count": 0,
        "percentage": 0
    },
    "group4": {
        "name": "failed",
        "count": 0,
        "percentage": 0
    },
    "meanNumberOfRequestsPerSecond": {
        "total": "0.1",
        "ok": "0.1",
        "ko": "-"
    }
}
    },"req_get-all-users10-26d08": {
        type: "REQUEST",
        name: "get all users1033",
path: "get all users1033",
pathFormatted: "req_get-all-users10-26d08",
stats: {
    "name": "get all users1033",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "156",
        "ok": "156",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "156",
        "ok": "156",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "156",
        "ok": "156",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "156",
        "ok": "156",
        "ko": "-"
    },
    "percentiles2": {
        "total": "156",
        "ok": "156",
        "ko": "-"
    },
    "percentiles3": {
        "total": "156",
        "ok": "156",
        "ko": "-"
    },
    "percentiles4": {
        "total": "156",
        "ok": "156",
        "ko": "-"
    },
    "group1": {
        "name": "t < 800 ms",
        "count": 1,
        "percentage": 100
    },
    "group2": {
        "name": "800 ms < t < 1200 ms",
        "count": 0,
        "percentage": 0
    },
    "group3": {
        "name": "t > 1200 ms",
        "count": 0,
        "percentage": 0
    },
    "group4": {
        "name": "failed",
        "count": 0,
        "percentage": 0
    },
    "meanNumberOfRequestsPerSecond": {
        "total": "0.1",
        "ok": "0.1",
        "ko": "-"
    }
}
    },"req_get-all-users10-cd9a1": {
        type: "REQUEST",
        name: "get all users1031",
path: "get all users1031",
pathFormatted: "req_get-all-users10-cd9a1",
stats: {
    "name": "get all users1031",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "160",
        "ok": "160",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "160",
        "ok": "160",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "160",
        "ok": "160",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "160",
        "ok": "160",
        "ko": "-"
    },
    "percentiles2": {
        "total": "160",
        "ok": "160",
        "ko": "-"
    },
    "percentiles3": {
        "total": "160",
        "ok": "160",
        "ko": "-"
    },
    "percentiles4": {
        "total": "160",
        "ok": "160",
        "ko": "-"
    },
    "group1": {
        "name": "t < 800 ms",
        "count": 1,
        "percentage": 100
    },
    "group2": {
        "name": "800 ms < t < 1200 ms",
        "count": 0,
        "percentage": 0
    },
    "group3": {
        "name": "t > 1200 ms",
        "count": 0,
        "percentage": 0
    },
    "group4": {
        "name": "failed",
        "count": 0,
        "percentage": 0
    },
    "meanNumberOfRequestsPerSecond": {
        "total": "0.1",
        "ok": "0.1",
        "ko": "-"
    }
}
    },"req_get-all-users10-4319b": {
        type: "REQUEST",
        name: "get all users1034",
path: "get all users1034",
pathFormatted: "req_get-all-users10-4319b",
stats: {
    "name": "get all users1034",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "168",
        "ok": "168",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "168",
        "ok": "168",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "168",
        "ok": "168",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "168",
        "ok": "168",
        "ko": "-"
    },
    "percentiles2": {
        "total": "168",
        "ok": "168",
        "ko": "-"
    },
    "percentiles3": {
        "total": "168",
        "ok": "168",
        "ko": "-"
    },
    "percentiles4": {
        "total": "168",
        "ok": "168",
        "ko": "-"
    },
    "group1": {
        "name": "t < 800 ms",
        "count": 1,
        "percentage": 100
    },
    "group2": {
        "name": "800 ms < t < 1200 ms",
        "count": 0,
        "percentage": 0
    },
    "group3": {
        "name": "t > 1200 ms",
        "count": 0,
        "percentage": 0
    },
    "group4": {
        "name": "failed",
        "count": 0,
        "percentage": 0
    },
    "meanNumberOfRequestsPerSecond": {
        "total": "0.1",
        "ok": "0.1",
        "ko": "-"
    }
}
    },"req_get-all-users10-bca05": {
        type: "REQUEST",
        name: "get all users1032",
path: "get all users1032",
pathFormatted: "req_get-all-users10-bca05",
stats: {
    "name": "get all users1032",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "149",
        "ok": "149",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "149",
        "ok": "149",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "149",
        "ok": "149",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "149",
        "ok": "149",
        "ko": "-"
    },
    "percentiles2": {
        "total": "149",
        "ok": "149",
        "ko": "-"
    },
    "percentiles3": {
        "total": "149",
        "ok": "149",
        "ko": "-"
    },
    "percentiles4": {
        "total": "149",
        "ok": "149",
        "ko": "-"
    },
    "group1": {
        "name": "t < 800 ms",
        "count": 1,
        "percentage": 100
    },
    "group2": {
        "name": "800 ms < t < 1200 ms",
        "count": 0,
        "percentage": 0
    },
    "group3": {
        "name": "t > 1200 ms",
        "count": 0,
        "percentage": 0
    },
    "group4": {
        "name": "failed",
        "count": 0,
        "percentage": 0
    },
    "meanNumberOfRequestsPerSecond": {
        "total": "0.1",
        "ok": "0.1",
        "ko": "-"
    }
}
    },"req_get-all-users10-f91cf": {
        type: "REQUEST",
        name: "get all users1038",
path: "get all users1038",
pathFormatted: "req_get-all-users10-f91cf",
stats: {
    "name": "get all users1038",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "160",
        "ok": "-",
        "ko": "160"
    },
    "maxResponseTime": {
        "total": "160",
        "ok": "-",
        "ko": "160"
    },
    "meanResponseTime": {
        "total": "160",
        "ok": "-",
        "ko": "160"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "160",
        "ok": "-",
        "ko": "160"
    },
    "percentiles2": {
        "total": "160",
        "ok": "-",
        "ko": "160"
    },
    "percentiles3": {
        "total": "160",
        "ok": "-",
        "ko": "160"
    },
    "percentiles4": {
        "total": "160",
        "ok": "-",
        "ko": "160"
    },
    "group1": {
        "name": "t < 800 ms",
        "count": 0,
        "percentage": 0
    },
    "group2": {
        "name": "800 ms < t < 1200 ms",
        "count": 0,
        "percentage": 0
    },
    "group3": {
        "name": "t > 1200 ms",
        "count": 0,
        "percentage": 0
    },
    "group4": {
        "name": "failed",
        "count": 1,
        "percentage": 100
    },
    "meanNumberOfRequestsPerSecond": {
        "total": "0.1",
        "ok": "-",
        "ko": "0.1"
    }
}
    },"req_get-all-users10-ba4d1": {
        type: "REQUEST",
        name: "get all users1036",
path: "get all users1036",
pathFormatted: "req_get-all-users10-ba4d1",
stats: {
    "name": "get all users1036",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "232",
        "ok": "232",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "232",
        "ok": "232",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "232",
        "ok": "232",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "232",
        "ok": "232",
        "ko": "-"
    },
    "percentiles2": {
        "total": "232",
        "ok": "232",
        "ko": "-"
    },
    "percentiles3": {
        "total": "232",
        "ok": "232",
        "ko": "-"
    },
    "percentiles4": {
        "total": "232",
        "ok": "232",
        "ko": "-"
    },
    "group1": {
        "name": "t < 800 ms",
        "count": 1,
        "percentage": 100
    },
    "group2": {
        "name": "800 ms < t < 1200 ms",
        "count": 0,
        "percentage": 0
    },
    "group3": {
        "name": "t > 1200 ms",
        "count": 0,
        "percentage": 0
    },
    "group4": {
        "name": "failed",
        "count": 0,
        "percentage": 0
    },
    "meanNumberOfRequestsPerSecond": {
        "total": "0.1",
        "ok": "0.1",
        "ko": "-"
    }
}
    },"req_search-users102-c6431": {
        type: "REQUEST",
        name: "search users1028",
path: "search users1028",
pathFormatted: "req_search-users102-c6431",
stats: {
    "name": "search users1028",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "158",
        "ok": "158",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "158",
        "ok": "158",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "158",
        "ok": "158",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "158",
        "ok": "158",
        "ko": "-"
    },
    "percentiles2": {
        "total": "158",
        "ok": "158",
        "ko": "-"
    },
    "percentiles3": {
        "total": "158",
        "ok": "158",
        "ko": "-"
    },
    "percentiles4": {
        "total": "158",
        "ok": "158",
        "ko": "-"
    },
    "group1": {
        "name": "t < 800 ms",
        "count": 1,
        "percentage": 100
    },
    "group2": {
        "name": "800 ms < t < 1200 ms",
        "count": 0,
        "percentage": 0
    },
    "group3": {
        "name": "t > 1200 ms",
        "count": 0,
        "percentage": 0
    },
    "group4": {
        "name": "failed",
        "count": 0,
        "percentage": 0
    },
    "meanNumberOfRequestsPerSecond": {
        "total": "0.1",
        "ok": "0.1",
        "ko": "-"
    }
}
    },"req_get-all-users10-07eda": {
        type: "REQUEST",
        name: "get all users1035",
path: "get all users1035",
pathFormatted: "req_get-all-users10-07eda",
stats: {
    "name": "get all users1035",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "166",
        "ok": "166",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "166",
        "ok": "166",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "166",
        "ok": "166",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "166",
        "ok": "166",
        "ko": "-"
    },
    "percentiles2": {
        "total": "166",
        "ok": "166",
        "ko": "-"
    },
    "percentiles3": {
        "total": "166",
        "ok": "166",
        "ko": "-"
    },
    "percentiles4": {
        "total": "166",
        "ok": "166",
        "ko": "-"
    },
    "group1": {
        "name": "t < 800 ms",
        "count": 1,
        "percentage": 100
    },
    "group2": {
        "name": "800 ms < t < 1200 ms",
        "count": 0,
        "percentage": 0
    },
    "group3": {
        "name": "t > 1200 ms",
        "count": 0,
        "percentage": 0
    },
    "group4": {
        "name": "failed",
        "count": 0,
        "percentage": 0
    },
    "meanNumberOfRequestsPerSecond": {
        "total": "0.1",
        "ok": "0.1",
        "ko": "-"
    }
}
    },"req_search-users102-0cbce": {
        type: "REQUEST",
        name: "search users1025",
path: "search users1025",
pathFormatted: "req_search-users102-0cbce",
stats: {
    "name": "search users1025",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "161",
        "ok": "161",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "161",
        "ok": "161",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "161",
        "ok": "161",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "161",
        "ok": "161",
        "ko": "-"
    },
    "percentiles2": {
        "total": "161",
        "ok": "161",
        "ko": "-"
    },
    "percentiles3": {
        "total": "161",
        "ok": "161",
        "ko": "-"
    },
    "percentiles4": {
        "total": "161",
        "ok": "161",
        "ko": "-"
    },
    "group1": {
        "name": "t < 800 ms",
        "count": 1,
        "percentage": 100
    },
    "group2": {
        "name": "800 ms < t < 1200 ms",
        "count": 0,
        "percentage": 0
    },
    "group3": {
        "name": "t > 1200 ms",
        "count": 0,
        "percentage": 0
    },
    "group4": {
        "name": "failed",
        "count": 0,
        "percentage": 0
    },
    "meanNumberOfRequestsPerSecond": {
        "total": "0.1",
        "ok": "0.1",
        "ko": "-"
    }
}
    },"req_get-all-users10-dcf71": {
        type: "REQUEST",
        name: "get all users1039",
path: "get all users1039",
pathFormatted: "req_get-all-users10-dcf71",
stats: {
    "name": "get all users1039",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "221",
        "ok": "-",
        "ko": "221"
    },
    "maxResponseTime": {
        "total": "221",
        "ok": "-",
        "ko": "221"
    },
    "meanResponseTime": {
        "total": "221",
        "ok": "-",
        "ko": "221"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "221",
        "ok": "-",
        "ko": "221"
    },
    "percentiles2": {
        "total": "221",
        "ok": "-",
        "ko": "221"
    },
    "percentiles3": {
        "total": "221",
        "ok": "-",
        "ko": "221"
    },
    "percentiles4": {
        "total": "221",
        "ok": "-",
        "ko": "221"
    },
    "group1": {
        "name": "t < 800 ms",
        "count": 0,
        "percentage": 0
    },
    "group2": {
        "name": "800 ms < t < 1200 ms",
        "count": 0,
        "percentage": 0
    },
    "group3": {
        "name": "t > 1200 ms",
        "count": 0,
        "percentage": 0
    },
    "group4": {
        "name": "failed",
        "count": 1,
        "percentage": 100
    },
    "meanNumberOfRequestsPerSecond": {
        "total": "0.1",
        "ok": "-",
        "ko": "0.1"
    }
}
    },"req_search-users102-e13b4": {
        type: "REQUEST",
        name: "search users1029",
path: "search users1029",
pathFormatted: "req_search-users102-e13b4",
stats: {
    "name": "search users1029",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "151",
        "ok": "151",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "151",
        "ok": "151",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "151",
        "ok": "151",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "151",
        "ok": "151",
        "ko": "-"
    },
    "percentiles2": {
        "total": "151",
        "ok": "151",
        "ko": "-"
    },
    "percentiles3": {
        "total": "151",
        "ok": "151",
        "ko": "-"
    },
    "percentiles4": {
        "total": "151",
        "ok": "151",
        "ko": "-"
    },
    "group1": {
        "name": "t < 800 ms",
        "count": 1,
        "percentage": 100
    },
    "group2": {
        "name": "800 ms < t < 1200 ms",
        "count": 0,
        "percentage": 0
    },
    "group3": {
        "name": "t > 1200 ms",
        "count": 0,
        "percentage": 0
    },
    "group4": {
        "name": "failed",
        "count": 0,
        "percentage": 0
    },
    "meanNumberOfRequestsPerSecond": {
        "total": "0.1",
        "ok": "0.1",
        "ko": "-"
    }
}
    },"req_search-users102-5e4e1": {
        type: "REQUEST",
        name: "search users1026",
path: "search users1026",
pathFormatted: "req_search-users102-5e4e1",
stats: {
    "name": "search users1026",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "175",
        "ok": "175",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "175",
        "ok": "175",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "175",
        "ok": "175",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "175",
        "ok": "175",
        "ko": "-"
    },
    "percentiles2": {
        "total": "175",
        "ok": "175",
        "ko": "-"
    },
    "percentiles3": {
        "total": "175",
        "ok": "175",
        "ko": "-"
    },
    "percentiles4": {
        "total": "175",
        "ok": "175",
        "ko": "-"
    },
    "group1": {
        "name": "t < 800 ms",
        "count": 1,
        "percentage": 100
    },
    "group2": {
        "name": "800 ms < t < 1200 ms",
        "count": 0,
        "percentage": 0
    },
    "group3": {
        "name": "t > 1200 ms",
        "count": 0,
        "percentage": 0
    },
    "group4": {
        "name": "failed",
        "count": 0,
        "percentage": 0
    },
    "meanNumberOfRequestsPerSecond": {
        "total": "0.1",
        "ok": "0.1",
        "ko": "-"
    }
}
    },"req_search-users102-23a0e": {
        type: "REQUEST",
        name: "search users1027",
path: "search users1027",
pathFormatted: "req_search-users102-23a0e",
stats: {
    "name": "search users1027",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "180",
        "ok": "180",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "180",
        "ok": "180",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "180",
        "ok": "180",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "180",
        "ok": "180",
        "ko": "-"
    },
    "percentiles2": {
        "total": "180",
        "ok": "180",
        "ko": "-"
    },
    "percentiles3": {
        "total": "180",
        "ok": "180",
        "ko": "-"
    },
    "percentiles4": {
        "total": "180",
        "ok": "180",
        "ko": "-"
    },
    "group1": {
        "name": "t < 800 ms",
        "count": 1,
        "percentage": 100
    },
    "group2": {
        "name": "800 ms < t < 1200 ms",
        "count": 0,
        "percentage": 0
    },
    "group3": {
        "name": "t > 1200 ms",
        "count": 0,
        "percentage": 0
    },
    "group4": {
        "name": "failed",
        "count": 0,
        "percentage": 0
    },
    "meanNumberOfRequestsPerSecond": {
        "total": "0.1",
        "ok": "0.1",
        "ko": "-"
    }
}
    },"req_get-all-users10-58b6c": {
        type: "REQUEST",
        name: "get all users1037",
path: "get all users1037",
pathFormatted: "req_get-all-users10-58b6c",
stats: {
    "name": "get all users1037",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "202",
        "ok": "202",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "202",
        "ok": "202",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "202",
        "ok": "202",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "202",
        "ok": "202",
        "ko": "-"
    },
    "percentiles2": {
        "total": "202",
        "ok": "202",
        "ko": "-"
    },
    "percentiles3": {
        "total": "202",
        "ok": "202",
        "ko": "-"
    },
    "percentiles4": {
        "total": "202",
        "ok": "202",
        "ko": "-"
    },
    "group1": {
        "name": "t < 800 ms",
        "count": 1,
        "percentage": 100
    },
    "group2": {
        "name": "800 ms < t < 1200 ms",
        "count": 0,
        "percentage": 0
    },
    "group3": {
        "name": "t > 1200 ms",
        "count": 0,
        "percentage": 0
    },
    "group4": {
        "name": "failed",
        "count": 0,
        "percentage": 0
    },
    "meanNumberOfRequestsPerSecond": {
        "total": "0.1",
        "ok": "0.1",
        "ko": "-"
    }
}
    },"req_search-users103-222c8": {
        type: "REQUEST",
        name: "search users1031",
path: "search users1031",
pathFormatted: "req_search-users103-222c8",
stats: {
    "name": "search users1031",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "154",
        "ok": "154",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "154",
        "ok": "154",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "154",
        "ok": "154",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "154",
        "ok": "154",
        "ko": "-"
    },
    "percentiles2": {
        "total": "154",
        "ok": "154",
        "ko": "-"
    },
    "percentiles3": {
        "total": "154",
        "ok": "154",
        "ko": "-"
    },
    "percentiles4": {
        "total": "154",
        "ok": "154",
        "ko": "-"
    },
    "group1": {
        "name": "t < 800 ms",
        "count": 1,
        "percentage": 100
    },
    "group2": {
        "name": "800 ms < t < 1200 ms",
        "count": 0,
        "percentage": 0
    },
    "group3": {
        "name": "t > 1200 ms",
        "count": 0,
        "percentage": 0
    },
    "group4": {
        "name": "failed",
        "count": 0,
        "percentage": 0
    },
    "meanNumberOfRequestsPerSecond": {
        "total": "0.1",
        "ok": "0.1",
        "ko": "-"
    }
}
    },"req_search-users103-67fa8": {
        type: "REQUEST",
        name: "search users1030",
path: "search users1030",
pathFormatted: "req_search-users103-67fa8",
stats: {
    "name": "search users1030",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "148",
        "ok": "148",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "148",
        "ok": "148",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "148",
        "ok": "148",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "148",
        "ok": "148",
        "ko": "-"
    },
    "percentiles2": {
        "total": "148",
        "ok": "148",
        "ko": "-"
    },
    "percentiles3": {
        "total": "148",
        "ok": "148",
        "ko": "-"
    },
    "percentiles4": {
        "total": "148",
        "ok": "148",
        "ko": "-"
    },
    "group1": {
        "name": "t < 800 ms",
        "count": 1,
        "percentage": 100
    },
    "group2": {
        "name": "800 ms < t < 1200 ms",
        "count": 0,
        "percentage": 0
    },
    "group3": {
        "name": "t > 1200 ms",
        "count": 0,
        "percentage": 0
    },
    "group4": {
        "name": "failed",
        "count": 0,
        "percentage": 0
    },
    "meanNumberOfRequestsPerSecond": {
        "total": "0.1",
        "ok": "0.1",
        "ko": "-"
    }
}
    },"req_get-all-users10-8960e": {
        type: "REQUEST",
        name: "get all users1041",
path: "get all users1041",
pathFormatted: "req_get-all-users10-8960e",
stats: {
    "name": "get all users1041",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "205",
        "ok": "-",
        "ko": "205"
    },
    "maxResponseTime": {
        "total": "205",
        "ok": "-",
        "ko": "205"
    },
    "meanResponseTime": {
        "total": "205",
        "ok": "-",
        "ko": "205"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "205",
        "ok": "-",
        "ko": "205"
    },
    "percentiles2": {
        "total": "205",
        "ok": "-",
        "ko": "205"
    },
    "percentiles3": {
        "total": "205",
        "ok": "-",
        "ko": "205"
    },
    "percentiles4": {
        "total": "205",
        "ok": "-",
        "ko": "205"
    },
    "group1": {
        "name": "t < 800 ms",
        "count": 0,
        "percentage": 0
    },
    "group2": {
        "name": "800 ms < t < 1200 ms",
        "count": 0,
        "percentage": 0
    },
    "group3": {
        "name": "t > 1200 ms",
        "count": 0,
        "percentage": 0
    },
    "group4": {
        "name": "failed",
        "count": 1,
        "percentage": 100
    },
    "meanNumberOfRequestsPerSecond": {
        "total": "0.1",
        "ok": "-",
        "ko": "0.1"
    }
}
    },"req_get-all-users10-53314": {
        type: "REQUEST",
        name: "get all users1040",
path: "get all users1040",
pathFormatted: "req_get-all-users10-53314",
stats: {
    "name": "get all users1040",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "149",
        "ok": "149",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "149",
        "ok": "149",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "149",
        "ok": "149",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "149",
        "ok": "149",
        "ko": "-"
    },
    "percentiles2": {
        "total": "149",
        "ok": "149",
        "ko": "-"
    },
    "percentiles3": {
        "total": "149",
        "ok": "149",
        "ko": "-"
    },
    "percentiles4": {
        "total": "149",
        "ok": "149",
        "ko": "-"
    },
    "group1": {
        "name": "t < 800 ms",
        "count": 1,
        "percentage": 100
    },
    "group2": {
        "name": "800 ms < t < 1200 ms",
        "count": 0,
        "percentage": 0
    },
    "group3": {
        "name": "t > 1200 ms",
        "count": 0,
        "percentage": 0
    },
    "group4": {
        "name": "failed",
        "count": 0,
        "percentage": 0
    },
    "meanNumberOfRequestsPerSecond": {
        "total": "0.1",
        "ok": "0.1",
        "ko": "-"
    }
}
    }
}

}

function fillStats(stat){
    $("#numberOfRequests").append(stat.numberOfRequests.total);
    $("#numberOfRequestsOK").append(stat.numberOfRequests.ok);
    $("#numberOfRequestsKO").append(stat.numberOfRequests.ko);

    $("#minResponseTime").append(stat.minResponseTime.total);
    $("#minResponseTimeOK").append(stat.minResponseTime.ok);
    $("#minResponseTimeKO").append(stat.minResponseTime.ko);

    $("#maxResponseTime").append(stat.maxResponseTime.total);
    $("#maxResponseTimeOK").append(stat.maxResponseTime.ok);
    $("#maxResponseTimeKO").append(stat.maxResponseTime.ko);

    $("#meanResponseTime").append(stat.meanResponseTime.total);
    $("#meanResponseTimeOK").append(stat.meanResponseTime.ok);
    $("#meanResponseTimeKO").append(stat.meanResponseTime.ko);

    $("#standardDeviation").append(stat.standardDeviation.total);
    $("#standardDeviationOK").append(stat.standardDeviation.ok);
    $("#standardDeviationKO").append(stat.standardDeviation.ko);

    $("#percentiles1").append(stat.percentiles1.total);
    $("#percentiles1OK").append(stat.percentiles1.ok);
    $("#percentiles1KO").append(stat.percentiles1.ko);

    $("#percentiles2").append(stat.percentiles2.total);
    $("#percentiles2OK").append(stat.percentiles2.ok);
    $("#percentiles2KO").append(stat.percentiles2.ko);

    $("#percentiles3").append(stat.percentiles3.total);
    $("#percentiles3OK").append(stat.percentiles3.ok);
    $("#percentiles3KO").append(stat.percentiles3.ko);

    $("#percentiles4").append(stat.percentiles4.total);
    $("#percentiles4OK").append(stat.percentiles4.ok);
    $("#percentiles4KO").append(stat.percentiles4.ko);

    $("#meanNumberOfRequestsPerSecond").append(stat.meanNumberOfRequestsPerSecond.total);
    $("#meanNumberOfRequestsPerSecondOK").append(stat.meanNumberOfRequestsPerSecond.ok);
    $("#meanNumberOfRequestsPerSecondKO").append(stat.meanNumberOfRequestsPerSecond.ko);
}
